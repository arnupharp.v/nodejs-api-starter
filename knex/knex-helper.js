const moment = require('moment');

const SHORTSTRING_LENGTH = 50;
const STRING_LENGTH = 255;
const LONGSTRING_LENGTH = 1000;

const { DB_PROVIDER } = process.env;

// eslint-disable-next-line
export function createFromSchemas(schemas) {
  return (table) => {
    schemas.forEach(({ name, type, unique, index, enu, nullable, defaultTo, pk, fk, fkTable }) => {
      let field;
      switch (type) {
        case 'incre':
          field = table.increments(name);
          break;
        case 'increref':
          field = table.integer(name).unsigned();
          break;
        case 'enum':
          field = table.enu(name, enu);
          break;
        case 'shortstring':
          field = table.string(name, SHORTSTRING_LENGTH);
          break;
        case 'string':
          field = table.string(name, STRING_LENGTH);
          break;
        case 'longstring':
          field = table.string(name, LONGSTRING_LENGTH);
          break;
        case 'text':
          field = table.text(name);
          break;
        case 'longtext':
            field = table.text(name, 'longtext');
            break;
        case 'date':
          field = table.date(name);
          break;          
        case 'timestamp':
          field = table.timestamp(name);
          break;
        case 'datetime':
          field = table.dateTime(name);
          break;
        case 'jsonb':
          if (DB_PROVIDER === 'postgresql') {
            field = table[type](name);
          } else {
            field = table.text(name, 'longtext');
          }
          break;
        case 'percent':
          field = table.decimal(name, 7, 4);
          break;
        case 'currency':
          field = table.decimal(name, 17, 8);
          break;
        case 'number':
          field = table.integer(name);
          break;
        case 'uint':
            field = table.integer(name).unsigned();
            break;
        case 'boolean':
          field = table.boolean(name);
          break;
        case 'tstzrange':
          field = table.specificType(name, 'tstzrange');
          break;
        case 'uuid':
          field = table.string(name, SHORTSTRING_LENGTH);
          break;
        default:
          // no op
          break;
      }

      if (pk === true) {
        field.primary();
      }

      if (unique === true) {
        field.unique();
      }

      if (index === true) {
        field.index();
      }

      if (nullable === false) {
        field.notNullable();
      } else {
        // if nullable is not specified or is true
        field.nullable();
      }

      if (typeof defaultTo !== 'undefined') {
        field.defaultTo(defaultTo);
      }

      if (fk && fkTable) {
        field.references(fk).inTable(fkTable).onUpdate('CASCADE');
        // .onDelete('SET NULL');
      }
    });
  };
}

export function alterEnum(tableName, columnName, enums) {
  const constraintName = `${tableName}_${columnName}_check`;
  return [
    `ALTER TABLE "${tableName}" DROP CONSTRAINT IF EXISTS "${constraintName}";`,
    `ALTER TABLE "${tableName}" ADD CONSTRAINT "${constraintName}" CHECK ("${columnName}" = ANY (ARRAY['${enums.join(
      "'::text, '",
    )}'::text]));`,
  ].join('\n');
}

export function alterFromSchemas(tableName, schemas, trx) {
  return (table) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const { name, type, enu, nullable, defaultTo, fk, fkTable, isNewColumn } of schemas) {
      let field;
      switch (type) {
        case 'enum':
          if (!isNewColumn) {
            // eslint-disable-next-line no-await-in-loop
            trx.raw(alterEnum(tableName, name, enu));
            field = table.text(name);
          } else {
            field = table.enu(name, enu);
          }
          break;
        case 'shortstring':
          field = table.string(name, SHORTSTRING_LENGTH);
          break;
        case 'string':
          field = table.string(name, STRING_LENGTH);
          break;
        case 'longstring':
          field = table.string(name, LONGSTRING_LENGTH);
          break;
        case 'text':
          field = table.text(name);
          break;
        case 'longtext':
            field = table.text(name, 'longtext');
            break;  
        case 'date':
          field = table.date(name);
          break;          
        case 'timestamp':
          field = table.timestamp(name);
          break;
        case 'datetime':
          field = table.dateTime(name);
          break;
        case 'jsonb':
          if (DB_PROVIDER === 'postgresql') {
            field = table[type](name);
          } else {
            field = table.text(name, 'longtext');
          }
          break;
        case 'percent':
          field = table.decimal(name, 7, 4);
          break;
        case 'currency':
          field = table.decimal(name, 17, 8);
          break;
        case 'number':
          field = table.integer(name);
          break;
        case 'uint':
          field = table.integer(name).unsigned();
          break;
        case 'boolean':
          field = table.boolean(name);
          break;
        case 'tstzrange':
          field = table.specificType(name, 'tstzrange');
          break;
        default:
          // no op
          break;
      }

      if (nullable === false) {
        field.notNullable();
      } else {
        // if nullable is not specified or is true
        field.nullable();
      }

      if (typeof defaultTo !== 'undefined') {
        field.defaultTo(defaultTo);
      }

      if (fk && fkTable) {
        field.references(fk).inTable(fkTable).onUpdate('CASCADE');
        // .onDelete('SET NULL');
      }

      if (!isNewColumn) {
        field.alter();
      }
    }
  };
}
