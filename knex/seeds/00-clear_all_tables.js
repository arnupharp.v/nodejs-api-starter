
// TODO: This file should be renamed to "99-clear_all_tables.js" on production to prevent from unintentionlly delete data.

const resetIdentityCommand = (tableName) => {
    const { DB_PROVIDER } = process.env;
    if (DB_PROVIDER === 'postgresql') {
        return `ALTER SEQUENCE "${tableName}_id_seq" RESTART WITH 1`;
    } else {
        return `ALTER TABLE ${tableName} AUTO_INCREMENT = 1`;
    }
}

export function seed(knex) {
    return knex('post_tags')
    
      .then(() => knex('post_tags').del())
      .then(() => knex.raw(resetIdentityCommand('post_tags')))
  
      .then(() => knex('tags').del())
      .then(() => knex.raw(resetIdentityCommand('tags')))
  
      .then(() => knex('posts').del())
      .then(() => knex.raw(resetIdentityCommand('posts')))
  
      .then(() => knex('categories').del())
      .then(() => knex.raw(resetIdentityCommand('categories')))

      .then(() => knex('user_roles').del())
      .then(() => knex.raw(resetIdentityCommand('user_roles')))

      .then(() => knex('user_consents').del())
      .then(() => knex.raw(resetIdentityCommand('user_consents')))
      
      .then(() => knex('users').del())
  
      .then(() => knex('roles').del())
      .then(() => knex.raw(resetIdentityCommand('roles')))

      .then(() => knex('consent_forms').del())
      .then(() => knex.raw(resetIdentityCommand('consent_forms')))
  }
  