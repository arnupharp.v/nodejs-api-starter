const { addDays, formatForDB, getCurrentTime } = require('../../src/helpers/datetime');
const { createPasswordHash } = require('../../src/modules/users/users-domain');
import randomstring from 'randomstring';

export function seed(knex) {
  const expiredDate = addDays(process.env.APP_CONSENT_EXPIRE_DAYS);
  const currentDate = getCurrentTime();
  const consentFormChecksum = 'c1048d3f9511d46d9965e6073cc39909';
  const adminId = 'b56db29f-acd0-4b45-a8c0-9fb8454e157d';
  const admins = [
    {
      id: adminId,
      gender: 'other',
      firstName: 'Admin',
      lastName: 'Admin',
      email: 'admin@local.com',
      consentFormChecksum,
      provider: 'local',
      passwordHash: createPasswordHash('admin'),
      refreshTokenId: randomstring.generate(50),
      refreshTokenSecret: randomstring.generate(20),
      status: 'verified',
      verifiedAt: currentDate,
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
  ];

  const adminRoles = [
    {
      userId: adminId,
      roleId: 1
    }
  ];

  const adminConsents = [
    {
      userId: adminId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'necessary',
      isConsented: true,
      expiredAt: formatForDB(expiredDate),
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
    {
      userId: adminId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'functional',
      isConsented: true,
      expiredAt: formatForDB(expiredDate),
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
    {
      userId: adminId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'analytics',
      isConsented: true,
      expiredAt: formatForDB(expiredDate),
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
    {
      userId: adminId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'marketing',
      isConsented: true,
      expiredAt: formatForDB(expiredDate),
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    }
  ];

  return knex('users')
    .then(() => knex('users').insert(admins))
    .then(() => knex('user_roles').insert(adminRoles))
    .then(() => knex('user_consents').insert(adminConsents));
}
