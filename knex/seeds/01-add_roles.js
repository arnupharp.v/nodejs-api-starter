const { getCurrentTime } = require('../../src/helpers/datetime');

export function seed(knex) {
  const currentDate = getCurrentTime();
  const roles = [
    {
      // id: 1,
      name: 'Administrators',
      description: 'Admin of system',
      scopes: JSON.stringify(['*.*.*']),
      roleType: 'system',
      status: 'active',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
  ];
  return knex('roles')
    .then(() => knex('roles').insert(roles));
}
