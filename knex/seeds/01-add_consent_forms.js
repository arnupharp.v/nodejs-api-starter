const { getCurrentTime } = require('../../src/helpers/datetime');
const { md5 } = require('../../src/helpers/security');
export function seed(knex) {
  const currentDate = getCurrentTime();
  const context = {
    consentItems: [{
      key: 'necessary',
      description: 'Necessary cookies (First Party Cookies) are sometimes called "strictly necessary" as without them we cannot provide the functionality that you need to use this website. For example, essential cookies help remember your preferences as you navigate through the online school.',
      isRequired: true
    }, {
      key: 'functional',
      description: 'During your interaction with this website, cookies are used to remember information you have entered or choices you make (such as your username or preferences for personalizing your account). These preferences are remembered, through the use of the persistent cookies, and the next time you use the Website you will not have to set them again.',
      isRequired: false
    }, {
      key: 'analytics',
      description: 'These cookies track information about visits to this Website so that we can make improvements and report our performance. For example: analyze User behavior so as to provide additional functionality or improve course contents. These cookies collect information about how visitors use the Website, which site or page they came from, the number of visits and how long a user stays on the Website.',
      isRequired: false
    }, {
      key: 'marketing',
      description: 'These cookies are used to deliver advertising materials relevant to you and your interests. They are also used to limit the number of times you see an advertisement as well as help measure the effectiveness of campaigns. They are usually placed by advertising networks we work with with our permission. They remember that you have visited a website and this information is shared with other organizations such as advertisers.',
      isRequired: false
    }]
  }

  const contextStr = JSON.stringify(context);
  const checksum = md5(contextStr.replace(/\s/g, ""));
  
  // checksum = c1048d3f9511d46d9965e6073cc39909

  const version = 1;
  const consentForms = [
    {
      // id: 1,
      consentVersion: version,
      title: 'Privacy policy version 1',
      context: contextStr,
      consentFormChecksum: checksum,
      status: 'active',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    }
  ];
  return knex('consent_forms')
    .then(() => knex('consent_forms').insert(consentForms));
}
