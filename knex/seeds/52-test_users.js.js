import { v4 as uuidv4 } from 'uuid';
import randomstring from 'randomstring';
import { padLeft } from '../../src/helpers/string';

const { getCurrentTime, addDays, formatForDB } = require('../../src/helpers/datetime');
const { createPasswordHash } = require('../../src/modules/users/users-domain');

// TODO: This seed file should be excluded on production.
export function seed(knex) {
  const currentDate = getCurrentTime();
  const expiredDate = addDays(process.env.APP_CONSENT_EXPIRE_DAYS);
  const consentFormChecksum = 'c1048d3f9511d46d9965e6073cc39909';

  let users = [];
  let userConsents = [];

  const passwordHash = createPasswordHash('1234');
  const expiredAt = formatForDB(expiredDate);

  for(let i = 1; i <= 10; i += 1) {

    const userId = uuidv4();

    users.push({
      id: userId,
      gender: 'other',
      firstName: `Test${i}`,
      lastName: `TestSurname${i}`,
      email: `test${i}@local.com`,
      consentFormChecksum,
      provider: 'local',
      passwordHash,
      refreshTokenId: randomstring.generate(50),
      refreshTokenSecret: randomstring.generate(20),
      status: 'verified',
      verifiedAt: currentDate,
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    });

    userConsents.push({
      userId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'necessary',
      isConsented: true,
      expiredAt,
      createdAt: currentDate,
      createdBy: 'system',
    });

    userConsents.push({
      userId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'functional',
      isConsented: true,
      expiredAt,
      createdAt: currentDate,
      createdBy: 'system',
    });

    userConsents.push({
      userId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'analytics',
      isConsented: true,
      expiredAt,
      createdAt: currentDate,
      createdBy: 'system',
    });

    userConsents.push({
      userId,
      consentFormId: 1,
      consentFormChecksum,
      consentItemKey: 'marketing',
      isConsented: true,
      expiredAt,
      createdAt: currentDate,
      createdBy: 'system',
    });
  }

  return knex('users')
    .then(() => knex('users').insert(users))
    .then(() => knex('user_consents').insert(userConsents));
}
