import { serializeJsonbValue } from '../../src/helpers/database';

const { getCurrentTime } = require('../../src/helpers/datetime');

export function seed(knex) {
  const currentDate = getCurrentTime();
  const posts = [];
  const tags = [
    {
      // id: 1,
      name: 'Tag 1',
      slug: 'tag-1',
    },
    {
      // id: 2,
      name: 'Tag 2',
      slug: 'tag-2',
    },
    {
      // id: 3,
      name: 'Tag 3',
      slug: 'tag-3',
    },
    {
      // id: 4,
      name: 'Tag 4',
      slug: 'tag-4',
    },
    {
      // id: 5,
      name: 'Tag 5',
      slug: 'tag-5',
    },
  ];

  const postTags = [];

  for(let i = 1; i <= 20; i += 1) {
    posts.push({
      // id: i,
      title: `Article ${i}`,
      postType: 'article',
      categoryId: (i % 3) + 1,
      status: 'published',
      publishedAt: currentDate,
      keywords: serializeJsonbValue(['keyword1', 'keyword2', `keyword${i}`]),
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      content: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Laoreet non curabitur gravida arcu. Sagittis nisl rhoncus mattis rhoncus urna neque viverra justo nec. Egestas maecenas pharetra convallis posuere morbi leo urna. Rhoncus est pellentesque elit ullamcorper. Neque laoreet suspendisse interdum consectetur libero id faucibus. Sit amet nisl purus in mollis nunc sed id semper. Mollis nunc sed id semper. Sit amet tellus cras adipiscing enim. Ipsum a arcu cursus vitae congue mauris. Pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae. Etiam erat velit scelerisque in dictum. Facilisi nullam vehicula ipsum a arcu cursus vitae congue mauris. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Elementum nibh tellus molestie nunc non blandit massa enim. Ridiculus mus mauris vitae ultricies leo. Felis donec et odio pellentesque diam volutpat commodo sed egestas. Aliquam ut porttitor leo a diam sollicitudin tempor id eu.

Gravida neque convallis a cras semper auctor neque vitae tempus. Elit eget gravida cum sociis natoque penatibus et magnis. Id ornare arcu odio ut sem. Feugiat pretium nibh ipsum consequat nisl vel pretium. Elit ullamcorper dignissim cras tincidunt. In vitae turpis massa sed elementum tempus egestas sed. Sit amet nulla facilisi morbi tempus iaculis urna id. Rhoncus est pellentesque elit ullamcorper. Non arcu risus quis varius quam quisque. Ut faucibus pulvinar elementum integer enim. A erat nam at lectus urna.

Nisl vel pretium lectus quam id leo. Urna nunc id cursus metus aliquam eleifend. Suspendisse ultrices gravida dictum fusce ut. Sit amet nisl purus in. Congue quisque egestas diam in arcu cursus euismod quis viverra. Aliquet eget sit amet tellus cras adipiscing enim eu turpis. Sit amet consectetur adipiscing elit ut aliquam purus. Tortor at auctor urna nunc id cursus metus. Ornare arcu odio ut sem nulla pharetra diam sit amet. Vestibulum morbi blandit cursus risus at ultrices mi tempus imperdiet. Ac auctor augue mauris augue neque gravida in. Enim facilisis gravida neque convallis a cras. Ultrices mi tempus imperdiet nulla malesuada pellentesque elit.

Et malesuada fames ac turpis egestas sed tempus urna. Venenatis urna cursus eget nunc scelerisque viverra. At imperdiet dui accumsan sit amet nulla facilisi morbi tempus. Netus et malesuada fames ac. Arcu felis bibendum ut tristique et egestas. Leo in vitae turpis massa sed elementum tempus egestas sed. Lacus vestibulum sed arcu non odio euismod. Turpis cursus in hac habitasse platea dictumst quisque sagittis. Pellentesque adipiscing commodo elit at imperdiet dui. Congue eu consequat ac felis. Justo nec ultrices dui sapien eget mi proin sed. Imperdiet massa tincidunt nunc pulvinar. Ornare suspendisse sed nisi lacus sed viverra.

Nunc non blandit massa enim nec dui nunc mattis. Purus gravida quis blandit turpis cursus in. Amet consectetur adipiscing elit ut. Consequat nisl vel pretium lectus quam. Integer vitae justo eget magna fermentum iaculis eu. Aliquam nulla facilisi cras fermentum. Parturient montes nascetur ridiculus mus mauris. Sed id semper risus in hendrerit gravida rutrum. Vitae congue eu consequat ac felis donec et. Tortor aliquam nulla facilisi cras fermentum odio eu. Odio aenean sed adipiscing diam donec adipiscing tristique risus nec. Tempus imperdiet nulla malesuada pellentesque elit eget. Ultrices eros in cursus turpis massa tincidunt dui ut. Gravida arcu ac tortor dignissim. Mattis pellentesque id nibh tortor id aliquet. Fusce id velit ut tortor pretium viverra suspendisse potenti nullam. Ac turpis egestas sed tempus urna. At consectetur lorem donec massa. Enim diam vulputate ut pharetra. Malesuada nunc vel risus commodo viverra maecenas accumsan lacus.
      `,
      tagNames: serializeJsonbValue(['Tag 1', `Tag ${(i % 5) + 1}`]),
      imageUrl: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Lorem_ipsum_design.svg/300px-Lorem_ipsum_design.svg.png',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    });

    postTags.push({
      postId: i,
      tagId: 1,
      ordering: 1
    });

    postTags.push({
      postId: i,
      tagId: (i % 5) + 1,
      ordering: 2
    });
  }

  return knex('posts')
    .then(() => knex('tags').insert(tags))
    .then(() => knex('posts').insert(posts))
    .then(() => knex('post_tags').insert(postTags));
}
