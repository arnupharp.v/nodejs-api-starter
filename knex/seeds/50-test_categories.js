const { getCurrentTime } = require('../../src/helpers/datetime');

export function seed(knex) {
  const currentDate = getCurrentTime();
  const data = [
    {
      // id: 1,
      name: 'Article Category 1',
      categoryGroup: 'article',
      status: 'active',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
    {
      // id: 2,
      name: 'Article Category 2',
      categoryGroup: 'article',
      status: 'active',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
    {
      // id: 3,
      name: 'Article Category 3',
      categoryGroup: 'article',
      status: 'active',
      createdAt: currentDate,
      createdBy: 'system',
      updatedAt: currentDate,
      updatedBy: 'system',
    },
  ];

  return knex('categories')
    .then(() => knex('categories').insert(data));
}
