/* eslint-disable */

const Client_MySQL = require('../node_modules/knex/lib/dialects/mysql/index');

class Client_MariaDB extends Client_MySQL {}

Object.assign(Client_MariaDB.prototype, {
  driverName: 'mariadb',

  _driver: function () {
    return require('mariadb/callback');
  },

  validateConnection: function (connection) {
    return connection.isValid();
  },
});

module.exports = Client_MariaDB;
