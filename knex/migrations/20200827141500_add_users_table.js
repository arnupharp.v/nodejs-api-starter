const { createFromSchemas } = require('../knex-helper');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'shortstring', pk: true, unique: true, index: true, nullable: false }, 
    { name: 'gender', type: 'shortstring' },  // ex. male, femal, other
    { name: 'firstName', type: 'string', index: true },
    { name: 'lastName', type: 'string', index: true },
    { name: 'email', type: 'string', index: true },
    { name: 'telephone', type: 'shortstring', index: true },
    { name: 'birthDate', type: 'date' },
    { name: 'consentFormChecksum', type: 'shortstring' },
    { name: 'profileImageUrl', type: 'longstring' },
    { name: 'provider', type: 'shortstring', index: true, nullable: false },  // ex. local, facebook, google
    { name: 'providerId', type: 'string', index: true },    
    { name: 'passwordHash', type: 'longstring' },
    { name: 'refreshTokenId', type: 'string', unique: true, index: true, nullable: false },
    {
      name: 'refreshTokenSecret',
      type: 'shortstring',
      unique: false,
      index: true,
      nullable: false,
    },
    { name: 'resetPasswordToken', type: 'string', index: true },
    {
      name: 'status',
      type: 'enum',
      enu: ['unverified', 'wait_verify', 'verified', 'locked', 'deleted'],
      nullable: false,
    },
    { name: 'verifiedAt', type: 'datetime' },
    { name: 'lastLoginAt', type: 'datetime' },
    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'shortstring' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'shortstring' },
  ];

  return knex.schema.createTable('users', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('users');
};
