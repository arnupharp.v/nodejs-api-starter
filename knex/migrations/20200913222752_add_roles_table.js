const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'name', type: 'string', unique: true, index: true, nullable: false },
    { name: 'description', type: 'longstring', unique: false, index: false, nullable: true },
    { name: 'roleType', type: 'shortstring' },
    { name: 'scopes', type: 'jsonb', unique: false, index: false, nullable: false },
    {
      name: 'status',
      type: 'enum',
      unique: false,
      index: false,
      nullable: false,
      enu: ['active', 'deleted'],
    },
    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'string' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'string' },
  ];

  return knex.schema.createTable('roles', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('roles');
};
