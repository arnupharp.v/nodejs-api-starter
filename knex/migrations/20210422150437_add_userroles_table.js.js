const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'userId', type: 'shortstring', index: true, fk: 'id', fkTable: 'users' },
    { name: 'roleId', type: 'uint', index: true, fk: 'id', fkTable: 'roles' },
  ];

  return knex.schema.createTable('user_roles', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('user_roles');
};
