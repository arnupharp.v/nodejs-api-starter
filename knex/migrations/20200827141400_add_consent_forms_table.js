const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'consentVersion', type: 'number', nullable: false, defaultTo: 1 },
    { name: 'title', type: 'string' },
    { name: 'context', type: 'jsonb' },   // Consent detail store here
    { name: 'consentFormChecksum', type: 'shortstring', index: true },    // md5(context.replace(/\s/g, "")
    {
      name: 'status',
      type: 'enum',
      enu: ['active', 'inactive'],
      nullable: false,
    },
    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'shortstring' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'shortstring' },
  ];

  return knex.schema.createTable('consent_forms', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('consent_forms');
};
