const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'title', type: 'longstring' },
    { name: 'description', type: 'text' },
    { name: 'content', type: 'text' },

    // eg., article, news, event, course, questionnaire
    { name: 'postType', type: 'shortstring', index: true },
    { name: 'categoryId', type: 'uint', index: true, fk: 'id', fkTable: 'categories' },
    {
      name: 'status',
      type: 'enum',
      enu: ['draft', 'published', 'unpublished', 'deleted'],
      nullable: false
    },
    { name: 'publishedAt', type: 'datetime' },
    { name: 'expiredAt', type: 'datetime' },
    { name: 'keywords', type: 'jsonb' },
    { name: 'tagNames', type: 'jsonb' },
    { name: 'imageUrl', type: 'longstring' },
    { name: 'url', type: 'longstring' },
    { name: 'context', type: 'jsonb' },   // Any other metadata will be stored here.

    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'shortstring' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'shortstring' },

  ];

  return knex.schema.createTable('posts', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('posts');
};
