const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'userId', type: 'shortstring', index: true, fk: 'id', fkTable: 'users' },
    { name: 'consentFormId', type: 'uint', index: true, fk: 'id', fkTable: 'consent_forms' },    
    { name: 'consentFormChecksum', type: 'shortstring', index: true, nullable: false },
    { name: 'consentItemKey', type: 'shortstring', index: true, nullable: false },
    { name: 'isConsented', type: 'boolean', nullable: false },
    { name: 'expiredAt', type: 'datetime' },
    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'shortstring' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'shortstring' }
  ];

  return knex.schema.createTable('user_consents', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('user_consents');
};
