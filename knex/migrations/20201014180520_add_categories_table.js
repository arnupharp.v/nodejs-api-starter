const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'name', type: 'string', nullable: false },
    { name: 'parentCategoryId', type: 'uint', index: true },    // In case of hierarchical categories
    { name: 'categoryGroup', type: 'string', index: true },      // Groupping a set of categories
    {
        name: 'status',
        type: 'enum',
        unique: false,
        index: false,
        nullable: false,
        enu: ['active', 'deleted'],
    },
    { name: 'createdAt', type: 'datetime' },
    { name: 'createdBy', type: 'string' },
    { name: 'updatedAt', type: 'datetime' },
    { name: 'updatedBy', type: 'string' },
  ];

  return knex.schema.createTable('categories', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('categories');
};
