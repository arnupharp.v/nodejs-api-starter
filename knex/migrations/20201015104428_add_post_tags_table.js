const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'postId', type: 'uint', index: true, fk: 'id', fkTable: 'posts' },
    { name: 'tagId', type: 'uint', index: true, fk: 'id', fkTable: 'tags' },
    { name: 'ordering', type: 'number' }
  ];

  return knex.schema.createTable('post_tags', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('post_tags');
};
