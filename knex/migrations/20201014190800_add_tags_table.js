const { createFromSchemas } = require('../knex-helper.js');

exports.up = function (knex) {
  const schemas = [
    { name: 'id', type: 'incre', unique: true, index: true, nullable: false },
    { name: 'name', type: 'string', nullable: false },  // ex. Social Network
    { name: 'slug', type: 'string', index: true, nullable: false },   // ex. /tags/social-network
  ];

  return knex.schema.createTable('tags', createFromSchemas(schemas));
};

exports.down = function (knex) {
  return knex.schema.dropTable('tags');
};
