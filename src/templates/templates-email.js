export function getSignupVerifyEmailTemplate({ firstName, lastName, url }) {
  const template = {
    subject: '[Boilerplate-API] Account verification email',
    body: `Hello ${firstName || ''} ${lastName || ''},<br/><br/>

        Thanks for getting started with our e-Learning platform.<br/><br/>
        
        We need a little more information to complete your registration. <br/>
        Please click the link below to confirm your email address:<br/>
        
        <a href='${url}' target='_blank'>${url}</a>
        `,
  };

  return template;
}

export function getResetPasswordEmailTemplate({ firstName, lastName, url }) {
  const template = {
    subject: '[Boilerplate-API] Password reset request',
    body: `Hello ${firstName || ''} ${lastName || ''},<br/><br/>

        There was a request to change your password.<br/><br/>
        
        If you did not make this request, just ignore this email. Otherwise, please click the link below to change your password: <br/>
        
        <a href='${url}' target='_blank'>${url}</a>
        `,
  };

  return template;
}

export function getOrderMessageEmailTemplate({ order, message }) {
  const template = {
    subject: `[Boilerplate-API] Message regarding to order number ${order.orderNumber}`,
    body: `Hello ${order.firstName || ''} ${order.lastName || ''},<br/><br/>

        ${message}
        `,
  };

  return template;
}
