import './setenv';
import express from 'express';
import logMiddleware from './middlewares/log-middleware';
import mainMiddleware from './middlewares/main-middleware';
import routeMiddleware from './middlewares/route-middleware';
import errorMiddleware from './middlewares/error-middleware';
import jwtMiddleware from './middlewares/jwt-middleware';
import idTokenMiddleware from './middlewares/id-token-middleware';
import multipartMiddleware from './middlewares/multipart-middleware';
import './connections/knex';

const app = express();

mainMiddleware(app);
logMiddleware(app);
idTokenMiddleware(app);
jwtMiddleware(app);
multipartMiddleware(app);
routeMiddleware(app);
errorMiddleware(app);

export default app;
