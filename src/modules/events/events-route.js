import express from 'express';
import {
  createEventController,
  updateEventController,
  getAllEventsController,
  deleteEventController,
  getEventByIdController,
} from './events-controller';

const router = express.Router();

router.get('/', getAllEventsController.handler);
router.get('/:id', getEventByIdController.handler);
router.post(
  '/',
  createEventController.checkPermission,
  createEventController.validator,
  createEventController.handler,
);
router.patch(
  '/:id',
  updateEventController.checkPermission,
  updateEventController.validator,
  updateEventController.handler,
);
router.delete('/:id', deleteEventController.checkPermission, deleteEventController.handler);

export default router;
