import { findAllEvents, createEvent, findEventById, updateEvent } from './events-data';
import {
  PAGINATION_DEFAULT_LIMIT,
  POST_STATUS_PUBLISHED,
  STATUS_DELETED,
} from '../../helpers/constant';
import { notFound } from '../../helpers/error';
import { applyPostTagsService } from '../posts/posts-service';

export async function getAllEventsService(option) {
  const opt = option ? { ...option } : { keyword: null };

  if (!opt.offset) opt.offset = 0;
  if (!opt.limit) opt.limit = PAGINATION_DEFAULT_LIMIT;
  if (!opt.sortBy) opt.sortBy = 'id';
  if (!opt.sortDirection) opt.sortDirection = 'asc';

  const result = await findAllEvents(opt);
  return result;
}
export async function getEventByIdService(id) {
  const data = await findEventById(id);
  return data;
}

export async function createEventService(
  postType,
  title,
  description,
  content,
  status,
  publishedAt,
  expiredAt,
  context,
  tagNames,
  catetoryId,
  createdBy,
) {
  const event = {
    postType: 'event',
    title,
    description,
    content,
    status: status || POST_STATUS_PUBLISHED,
    publishedAt,
    expiredAt,
    context: JSON.stringify(context || {}),
    tagNames: JSON.stringify(tagNames || []),
    catetoryId,
  };

  const created = await createEvent(event, createdBy);
  await applyPostTagsService(created.id, tagNames || []);

  return created;
}

export async function updateEventService(
  id,
  postType,
  title,
  description,
  content,
  status,
  publishedAt,
  expiredAt,
  context,
  tagNames,
  catetoryId,
  updatedBy,
) {
  const existing = await findEventById(id);
  if (!existing) throw notFound();

  const saveData = {
    postType: 'event',
    title,
    description,
    content,
    status: status || POST_STATUS_PUBLISHED,
    publishedAt,
    expiredAt,
    context: JSON.stringify(context || {}),
    tagNames: JSON.stringify(tagNames || []),
    catetoryId,
  };

  await updateEvent(id, saveData, updatedBy);
  await applyPostTagsService(id, tagNames || []);

  const updated = await getEventByIdService(id);
  return updated;
}

export async function deleteEventService(id, deletedBy) {
  const existing = await findEventById(id);
  if (!existing) throw notFound();

  await updateEvent(id, { status: STATUS_DELETED }, deletedBy);
  return true;
}

export async function updateEventImageUrlService(id, imageUrl, updatedBy) {
  const existing = await findEventById(id);
  if (!existing) throw notFound();

  const event = {
    imageUrl,
  };

  await updateEvent(id, event, updatedBy);
  const updated = await findEventById(id);
  return updated;
}
