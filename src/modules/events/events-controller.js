import { body } from 'express-validator';
import createHandler from '../../helpers/async-controller';
import { send } from '../../helpers/response';
import {
  getAllEventsService,
  createEventService,
  deleteEventService,
  getEventByIdService,
  updateEventService,
  updateEventImageUrlService,
} from './events-service';
import { validate } from '../../helpers/validator';
import { uploadFile } from '../../helpers/file-upload';
import { canReadUnpublished } from '../admins/admins-domain';
import { forbidden } from '../../helpers/error';
import { checkPermission } from '../../helpers/auth';

export const getAllEventsController = {
  handler: createHandler(async (req, res) => {
    const { keyword, offset, limit, startDate, endDate } = req.query;
    const includeUnpublished = canReadUnpublished(req.credential);
    const result = await getAllEventsService({
      offset,
      limit,
      filter: {
        keyword,
        startDate,
        endDate,
      },
    });

    if (!includeUnpublished) {
      result.data = result.data.filter((post) => post.status === 'published');
    }

    if (result) {
      send({ code: 200, data: result.data, meta: result.meta }, req, res);
    } else {
      send({ code: 400 }, req, res);
    }
  }),
};
export const createEventController = {
  checkPermission: checkPermission(['events.*.create']),
  validator: validate([
    body('title').notEmpty().withMessage('is required'),
    body('description').notEmpty().withMessage('is required'),
    body('context').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      catetoryId,
    } = req.body;
    const { credential } = req;
    let created = await createEventService(
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      catetoryId,
      credential.email,
    );
    if (req.files && req.files.thumbnail) {
      const url = await uploadFile(req.files.thumbnail, {
        fileKey: `posts_${created.id}`,
        fileGroup: 'posts',
      });
      if (url) {
        created = await updateEventImageUrlService(created.id, url);
      }
    }
    send({ code: 200, data: created }, req, res);
  }),
};

export const updateEventController = {
  checkPermission: checkPermission(['events.*.update']),
  validator: validate([
    body('title').notEmpty().withMessage('is required'),
    body('description').notEmpty().withMessage('is required'),
    body('context').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      catetoryId,
    } = req.body;
    const { credential } = req;
    let updated = await updateEventService(
      req.params.id,
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      catetoryId,
      credential.email,
    );
    if (req.files && req.files.thumbnail) {
      const url = await uploadFile(req.files.thumbnail, {
        fileKey: `posts_${req.params.id}`,
        fileGroup: 'posts',
      });
      if (url) {
        updated = await updateEventImageUrlService(updated.id, url);
      }
    }
    send({ code: 200, data: updated }, req, res);
  }),
};

export const deleteEventController = {
  checkPermission: checkPermission(['events.*.delete']),
  handler: createHandler(async (req, res) => {
    const deleted = await deleteEventService(req.params.id);
    send({ code: 200, data: deleted }, req, res);
  }),
};

export const getEventByIdController = {
  handler: createHandler(async (req, res) => {
    const includeUnpublished = canReadUnpublished(req.credential);
    const post = await getEventByIdService(req.params.id, includeUnpublished);
    if (post.status !== 'published' && !includeUnpublished) {
      throw forbidden();
    }

    if (post) {
      send({ code: 200, data: post }, req, res);
    } else {
      send({ code: 404 }, req, res);
    }
  }),
};
