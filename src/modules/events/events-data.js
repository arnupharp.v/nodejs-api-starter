import { postFields, withForeignTables } from '../posts/posts-data';
import PostModel from '../posts/posts-model';
import { databaseError } from '../../helpers/error';
import { queryRangeWithModifier } from '../../helpers/data-model';
import { STATUS_DELETED } from '../../helpers/constant';
import { getCurrentTime } from '../../helpers/datetime';

export async function createEvent(event, createdBy) {
  try {
    const saveData = {
      ...event,
      createdAt: getCurrentTime(),
      createdBy,
      updatedAt: getCurrentTime(),
      updatedBy: createdBy,
    };

    const result = await PostModel.query().insert(saveData);
    return result;
  } catch (error) {
    throw databaseError(error);
  }
}
export async function updateEvent(id, post, updatedBy) {
  try {
    const saveData = {
      ...post,
      updatedAt: getCurrentTime(),
      updatedBy,
    };

    const result = await PostModel.query().patch(saveData).where({ id });
    return result;
  } catch (error) {
    throw databaseError(error);
  }
}
export async function findEventById(id) {
  try {
    const posts = await PostModel.query().where({ id }).andWhereNot({ status: 'deleted' });

    return posts.length > 0 ? posts[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findAllEvents(option) {
  try {
    const condBuilder = (builder) => {
      builder.whereNot('posts.status', STATUS_DELETED);
      builder.where({ postType: 'event' });

      if (option && option.filter) {
        const { status, keyword, startDate, endDate } = option.filter;
        if (status) builder.where({ status });
        if (keyword) {
          builder
            .where('title', 'ilike', `%${keyword}%`)
            .orWhere('description', 'ilike', `%${keyword}%`)
            .orWhereRaw(`REPLACE(CAST("tagNames" as text), '"', '') like ?`, [`%${keyword}%`])
            .orWhereRaw(`context->>'location' like ?`, [`%${keyword}%`]);
        }

        if (startDate && endDate) {
          builder.whereRaw(
            `(  (CAST(context->>'startDate' AS date) <= CAST(? AS DATE) AND CAST(? AS DATE) <= CAST(context->>'endDate' AS date))
                OR (CAST(context->>'startDate' AS date) <= CAST(? AS DATE) AND CAST(? AS DATE) <= CAST(context->>'endDate' AS date))
                OR (CAST(? AS DATE) <= CAST(context->>'startDate' AS date) AND CAST(context->>'startDate' AS date) <= CAST(? AS DATE)) 
                OR (CAST(? AS DATE) <= CAST(context->>'endDate' AS date) AND CAST(context->>'endDate' AS date) <= CAST(? AS DATE))  )`,
            [startDate, startDate, endDate, endDate, startDate, endDate, startDate, endDate],
          );
        } else if (startDate) {
          builder.whereRaw(`CAST(context->>'startDate' AS date) >= ?`, [startDate]);
        } else if (endDate) {
          builder.whereRaw(`CAST(context->>'endDate' AS date) <= ?`, [endDate]);
        }
      }
    };

    return queryRangeWithModifier(
      PostModel,
      withForeignTables,
      null,
      postFields.concat(['categories.name as categoryName']),
      condBuilder,
      option,
    );
  } catch (error) {
    throw databaseError(error);
  }
}
