import knex from '../../connections/knex';
import { databaseError } from '../../helpers/error';
import { STATUS_DELETED } from '../../helpers/constant';
import { getLikeOperator, knexQueryRange, knexQueryRangeWithAlias } from '../../helpers/knex-query';

const TABLE_NAME = 'roles';

export async function getRoleById(id) {
  try {
    const roles = await knex(TABLE_NAME).where({ id }).whereNot('status', STATUS_DELETED);
    return roles.length > 0 ? roles[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getRolesByIds(roleIds) {
  try {
    const roles = await knex(TABLE_NAME).whereIn('id', roleIds);
    return roles;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getRoleByName(name) {
  try {
    const roles = await knex(TABLE_NAME).where({ name }).whereNot('status', STATUS_DELETED);
    return roles.length > 0 ? roles[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getAllRoles({ offset, limit, filter, sorts }) {
  try {
    const conBuilders = [];

    conBuilders.push((builder) => {
      builder.whereNot('status', STATUS_DELETED);
    });

    if (filter) {
      const { keyword } = filter;
      if (keyword) {
        conBuilders.push((builder) => {
          builder
            .where('name', getLikeOperator(), `%${keyword}%`)
            .orWhere('description', getLikeOperator(), `%${keyword}%`);
        });
      }
    }

    return knexQueryRange(TABLE_NAME, conBuilders, { offset, limit, sorts });
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createRole(data) {
  try {
    return knex(TABLE_NAME)
      .insert(data)
      .then((ids) => {
        const id = ids.length ? ids[0] : 0;
        return getRoleById(id);
      });
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateRole(id, data) {
  try {
    await knex(TABLE_NAME).where({ id }).update(data);
    return getRoleById(id);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getRolesByUserId(userId) {
  try {
    // Obtain list of roles assigned to users
    const roleAlias = {
      name: 'roles_alias',
      query: `SELECT r.*
      FROM roles r
        INNER JOIN user_roles ur ON ur."roleId" = r.id
      WHERE ur."userId" = ? AND r.status <> ? `,
      params: [userId, STATUS_DELETED],
    };

    return knexQueryRangeWithAlias(roleAlias, [], { offset: 0, limit: 100, sorts: [] });
  } catch (error) {
    throw databaseError(error);
  }
}
