import express from 'express';

import {
  getRoleByIdController,
  createRoleController,
  updateRoleController,
  getAllRolesController,
  deleteRoleController,
} from './roles-controller';

const router = express.Router();

router.get('/:id', getRoleByIdController.checkPermission, getRoleByIdController.handler);
router.get('/', getAllRolesController.checkPermission, getAllRolesController.handler);
router.post(
  '/',
  createRoleController.checkPermission,
  createRoleController.validator,
  createRoleController.handler,
);
router.patch(
  '/:id',
  updateRoleController.checkPermission,
  updateRoleController.validator,
  updateRoleController.handler,
);
router.delete('/:id', deleteRoleController.checkPermission, deleteRoleController.handler);

export default router;
