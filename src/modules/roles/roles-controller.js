import { body } from 'express-validator';
import createHandler from '../../helpers/async-controller';
import { validate } from '../../helpers/validator';
import { send } from '../../helpers/response';
import { errorMessage, notFound } from '../../helpers/error';
import {
  createRoleService,
  updateRoleService,
  deleteRoleByIdService,
  getRoleByIdService,
  getAllRolesService,
} from './roles-service';
import { checkPermission } from '../../helpers/auth';

export const createRoleController = {
  checkPermission: checkPermission(['roles.*.create']),
  validator: validate([
    body('name').notEmpty().withMessage('is required'),
    body('scopes').isArray({ min: 1 }).withMessage('is not a valid array'),
  ]),
  handler: createHandler(async (req, res) => {
    const { name, description, scopes } = req.body;
    const result = await createRoleService({
      name,
      description,
      scopes,
      createdBy: req.credential.id,
    });

    if (result) {
      send({ code: 200, data: result }, req, res);
    } else {
      send({ code: 400, errorMessage: errorMessage.INTERNAL_ERROR }, req, res);
    }
  }),
};

export const updateRoleController = {
  checkPermission: checkPermission(['roles.*.update']),
  validator: validate([
    body('name').notEmpty().withMessage('is required'),
    body('scopes').isArray({ min: 1 }).withMessage('is not a valid array'),
  ]),
  handler: createHandler(async (req, res) => {
    const role = await getRoleByIdService(req.params.id);
    if (!role) throw notFound();

    const { name, description, scopes } = req.body;
    const result = await updateRoleService(role.id, { name, description, scopes });

    send({ code: 200, data: result }, req, res);
  }),
};

export const deleteRoleController = {
  checkPermission: checkPermission(['roles.*.delete']),
  handler: createHandler(async (req, res) => {
    const role = await getRoleByIdService(req.params.id);
    if (!role) throw notFound();

    await deleteRoleByIdService(role.id);
    send({ code: 200, data: true }, req, res);
  }),
};

export const getRoleByIdController = {
  checkPermission: checkPermission(['roles.*.read']),
  handler: createHandler(async (req, res) => {
    const role = await getRoleByIdService(req.params.id);
    if (!role) throw notFound();
    send({ code: 200, data: role }, req, res);
  }),
};

export const getAllRolesController = {
  checkPermission: checkPermission(['roles.*.list']),
  handler: createHandler(async (req, res) => {
    const { keyword, offset, limit } = req.query;
    const filter = {
      keyword,
    };

    const result = await getAllRolesService({
      offset,
      limit,
      filter,
    });

    if (result) {
      send({ code: 200, data: result.data, meta: result.meta }, req, res);
    } else {
      send({ code: 400 }, req, res);
    }
  }),
};

export async function getScopes(req, res) {
  const scopes = [
    {
      name: '*.*.*',
      description: 'จัดการข้อมูลได้ทั้งหมด (Full Control)',
    },
    {
      name: 'manage_user',
      description: 'จัดการข้อมูลผู้ใช้งานระบบ (User)',
    },
    {
      name: 'manage_member',
      description: 'จัดการข้อมูลสมาชิก (Member)',
    },
    {
      name: 'manage_static_page',
      description: 'จัดการข้อมูลหน้าเพจต่างๆ เช่น หน้าแสดงข้อมูลการติดต่อ',
    },
    {
      name: 'manage_curriculumn',
      description: 'จัดการข้อมูลหลักสูตร',
    },
    {
      name: 'manage_announcement',
      description: 'จัดการข้อมูลข่าวประกาศ',
    },
    {
      name: 'manage_event',
      description: 'จัดการข้อมูลกิจกรรม',
    },
    {
      name: 'manage_document',
      description: 'จัดการข้อมูลไฟล์คู่มือต่างๆ',
    },
    {
      name: 'manage_questionnaire',
      description: 'จัดการข้อมูลแบบสอบถาม',
    },
  ];

  send({ code: 200, data: scopes }, req, res);
}
