import {
  getRoleById,
  getRoleByName,
  createRole,
  updateRole,
  getRolesByUserId,
  getRolesByIds,
  getAllRoles,
} from './roles-data';
import { getCurrentTime } from '../../helpers/datetime';
import { notFound, validationError, forbidden } from '../../helpers/error';
import {
  ROLE_TYPE_CUSTOM,
  ROLE_TYPE_SYSTEM,
  PAGINATION_DEFAULT_LIMIT,
  STATUS_DELETED,
  STATUS_ACTIVE,
} from '../../helpers/constant';
import { deserializeJsonbValue, serializeJsonbValue } from '../../helpers/database';

export async function getRoleByIdService(id) {
  const data = await getRoleById(id);
  if (data) {
    data.scopes = deserializeJsonbValue(data.scopes);
  }
  return data;
}

export async function getRoleByNameService(name) {
  const data = await getRoleByName(name);
  if (data) {
    data.scopes = deserializeJsonbValue(data.scopes);
  }
  return data;
}

export async function getRolesByIdsService(roleIds) {
  const roles = await getRolesByIds(roleIds);
  for (let i = 0; i < roles.length; i += 1) {
    roles[i].scopes = deserializeJsonbValue(roles[i].scopes);
  }

  return roles;
}

export async function getAllRolesService(option) {
  const opt = option ? { ...option } : { keyword: null };

  if (!opt.offset) opt.offset = 0;
  if (!opt.limit) opt.limit = PAGINATION_DEFAULT_LIMIT;
  if (!opt.sortBy) opt.sortBy = 'name';
  if (!opt.sortDirection) opt.sortDirection = 'asc';

  const roles = await getAllRoles(opt);
  for (let i = 0; i < roles.data.length; i += 1) {
    roles.data[i].scopes = deserializeJsonbValue(roles.data[i].scopes);
  }

  return roles;
}

export async function createRoleService({ name, description, scopes, createdBy }) {
  const now = getCurrentTime();
  let saveData = {
    name,
    description,
    scopes: serializeJsonbValue(scopes),
    status: STATUS_ACTIVE,
    roleType: ROLE_TYPE_CUSTOM,
    createdAt: now,
    createdBy,
    updatedAt: now,
    updatedBy: createdBy,
  };

  const existing = await getRoleByName(saveData.name);
  if (existing) throw validationError('Role name is already taken.');

  saveData = await createRole(saveData);
  saveData.scopes = deserializeJsonbValue(saveData.scopes);

  return saveData;
}

export async function updateRoleService(id, { name, description, scopes, updatedBy }) {
  const now = getCurrentTime();
  const saveData = {
    name,
    description,
    scopes: serializeJsonbValue(scopes),
    updatedAt: now,
    updatedBy,
  };

  const existing = await getRoleByName(saveData.name);
  if (existing && existing.id !== id) throw validationError('Role name is already taken.');

  const updated = await updateRole(id, saveData);
  updated.scopes = deserializeJsonbValue(updated.scopes);

  return updated;
}

export async function deleteRoleByIdService(id, deletedBy) {
  const role = await getRoleById(id);
  if (!role) throw notFound();

  if (role.roleType === ROLE_TYPE_SYSTEM) throw forbidden();

  const now = getCurrentTime();
  let saveData = {
    updatedAt: now,
    updatedBy: deletedBy,
    name: `${role.name}_${new Date().getTime()}`, // Append random string to make it unique so we can add role with this name again.
    status: STATUS_DELETED,
  };

  saveData = await updateRole(id, saveData);
  return saveData;
}

export async function getRolesByUserIdService(userId) {
  const roles = await getRolesByUserId(userId);
  for (let i = 0; i < roles.data.length; i += 1) {
    roles.data[i].scopes = deserializeJsonbValue(roles.data[i].scopes);
  }

  return roles;
}
