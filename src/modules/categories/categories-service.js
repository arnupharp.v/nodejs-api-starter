import * as CategoryData from './categories-data';
import { PAGINATION_DEFAULT_LIMIT, STATUS_DELETED } from '../../helpers/constant';
import { notFound } from '../../helpers/error';

export async function getCategoryByIdService(id) {
  const data = await CategoryData.findCategoryById(id);
  return data;
}

export async function getCategoriesByIdsService(categoryIds) {
  const data = await CategoryData.findCategorysByIds(categoryIds);
  return data;
}

export async function getAllCategoriesService(option) {
  const opt = option ? { ...option } : { keyword: null };

  if (!opt.offset) opt.offset = 0;
  if (!opt.limit) opt.limit = PAGINATION_DEFAULT_LIMIT;
  if (!opt.sortBy) opt.sortBy = 'name';
  if (!opt.sortDirection) opt.sortDirection = 'asc';

  const data = await CategoryData.findAllCategories(opt);
  return data;
}

export async function createCategoryService(name, parentCategoryId, categoryGroup, createdBy) {
  const saveData = {
    name,
    parentCategoryId,
    categoryGroup,
  };

  const data = await CategoryData.createCategory(saveData, createdBy);
  return data;
}

export async function updateCategoryService(id, name, parentCategoryId, categoryGroup, updatedBy) {
  const saveData = {
    name,
    parentCategoryId,
    categoryGroup,
  };

  const existing = await CategoryData.findCategoryById(id);
  if (!existing) throw notFound();

  await CategoryData.updateCategory(id, saveData, updatedBy);
  const updated = await CategoryData.findCategoryById(id);

  return updated;
}

export async function deleteCategoryService(id, deletedBy) {
  const saveData = {
    status: STATUS_DELETED,
  };

  const existing = await CategoryData.findCategoryById(id);
  if (!existing) throw notFound();

  await CategoryData.updateCategory(id, saveData, deletedBy);
  return true;
}
