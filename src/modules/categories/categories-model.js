import { Model } from 'objection';

class CategoryModel extends Model {
  static get tableName() {
    return 'categories';
  }

  static get idColumn() {
    return 'id';
  }
}

module.exports = CategoryModel;
