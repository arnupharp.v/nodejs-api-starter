import { v4 as uuidv4 } from 'uuid';
import CategoryModel from './categories-model';
import { databaseError } from '../../helpers/error';
import { getCurrentTime } from '../../helpers/datetime';
import { STATUS_ACTIVE, STATUS_DELETED } from '../../helpers/constant';
import { queryRange } from '../../helpers/data-model';

export async function createCategory(category, createdBy) {
  try {
    const saveData = {
      ...category,
      status: STATUS_ACTIVE,
      id: uuidv4(),
      createdAt: getCurrentTime(),
      createdBy,
      updatedAt: getCurrentTime(),
      updatedBy: createdBy,
    };

    const result = await CategoryModel.query().insert(saveData);
    return result;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateCategory(id, category, updatedBy) {
  try {
    const saveData = {
      ...category,
      updatedAt: getCurrentTime(),
      updatedBy,
    };

    const result = await CategoryModel.query().patch(saveData).where({ id });
    return result;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findCategoryById(id) {
  try {
    const categories = await CategoryModel.query().where({ id }).whereNot('status', STATUS_DELETED);

    return categories.length > 0 ? categories[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findCategorysByIds(categoryIds) {
  try {
    const categories = await CategoryModel.query()
      .whereIn('id', categoryIds)
      .whereNot('status', STATUS_DELETED);
    return categories;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findCategoryByGroup(categoryGroup) {
  try {
    const categories = await CategoryModel.query()
      .where({ categoryGroup })
      .whereNot('status', STATUS_DELETED);
    return categories;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findCategoryByParent(parentCategoryId) {
  try {
    const categories = await CategoryModel.query()
      .where({ parentCategoryId })
      .whereNot('status', STATUS_DELETED);
    return categories;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function findAllCategories(option) {
  try {
    const condBuilder = (builder) => {
      builder.whereNot('status', STATUS_DELETED);
      if (option && option.filter) {
        const { keyword, parentCategoryId, categoryGroup } = option.filter;

        if (keyword) builder.where('name', 'ilike', `%${keyword}%`);
        if (parentCategoryId) builder.where({ parentCategoryId });
        if (categoryGroup) builder.where({ categoryGroup });
      }
    };

    return queryRange(CategoryModel, condBuilder, option);
  } catch (error) {
    throw databaseError(error);
  }
}
