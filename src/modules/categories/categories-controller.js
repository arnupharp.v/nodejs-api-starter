import { body } from 'express-validator';
import createHandler from '../../helpers/async-controller';
import { validate } from '../../helpers/validator';
import { send } from '../../helpers/response';
import {
  createCategoryService,
  updateCategoryService,
  deleteCategoryService,
  getCategoryByIdService,
  getAllCategoriesService,
} from './categories-service';

export const createCategoryController = {
  validator: validate([body('name').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { name, parentCategoryId, categoryGroup } = req.body;
    const { credential } = req;
    const created = await createCategoryService(
      name,
      parentCategoryId,
      categoryGroup,
      credential.email,
    );
    send({ code: 200, data: created }, req, res);
  }),
};

export const updateCategoryController = {
  validator: validate([body('name').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { name, parentCategoryId, categoryGroup } = req.body;
    const { credential } = req;
    const updated = await updateCategoryService(
      req.params.id,
      name,
      parentCategoryId,
      categoryGroup,
      credential.email,
    );
    send({ code: 200, data: updated }, req, res);
  }),
};

export const deleteCategoryController = {
  handler: createHandler(async (req, res) => {
    const deleted = await deleteCategoryService(req.params.id);
    send({ code: 200, data: deleted }, req, res);
  }),
};

export const getCategoryByIdController = {
  handler: createHandler(async (req, res) => {
    const post = await getCategoryByIdService(req.params.id);
    if (post) {
      send({ code: 200, data: post }, req, res);
    } else {
      send({ code: 404 }, req, res);
    }
  }),
};

export const getAllCategoriesController = {
  handler: createHandler(async (req, res) => {
    const { keyword, offset, limit, parentCategoryId, categoryGroup } = req.query;
    const result = await getAllCategoriesService({
      offset,
      limit,
      filter: {
        keyword,
        parentCategoryId,
        categoryGroup,
      },
    });

    if (result) {
      send({ code: 200, data: result.data, meta: result.meta }, req, res);
    } else {
      send({ code: 400 }, req, res);
    }
  }),
};
