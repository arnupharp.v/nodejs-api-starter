import express from 'express';
import {
  createCategoryController,
  updateCategoryController,
  deleteCategoryController,
  getCategoryByIdController,
  getAllCategoriesController,
} from './categories-controller';

const router = express.Router();

router.get('/', getAllCategoriesController.handler);
router.get('/:id', getCategoryByIdController.handler);
router.post('/', createCategoryController.validator, createCategoryController.handler);
router.patch('/:id', updateCategoryController.validator, updateCategoryController.handler);
router.delete('/:id', deleteCategoryController.handler);

export default router;
