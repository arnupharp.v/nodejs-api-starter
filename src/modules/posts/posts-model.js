import { Model } from 'objection';

class PostModel extends Model {
  static get tableName() {
    return 'posts';
  }

  static get idColumn() {
    return 'id';
  }
}

module.exports = PostModel;
