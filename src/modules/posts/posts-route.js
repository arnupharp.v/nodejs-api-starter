import express from 'express';
import { getPostByIdController, getAllPostsController } from './posts-controller';

const router = express.Router();

router.get('/', getAllPostsController.handler);
router.get('/:id', getPostByIdController.handler);

export default router;
