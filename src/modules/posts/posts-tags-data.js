import knex from '../../connections/knex';
import { databaseError } from '../../helpers/error';

const TABLE_NAME = 'post_tags';

export async function createPostTags(tags) {
  try {
    return knex(TABLE_NAME).insert(tags);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function clearPostTagsById(postId) {
  try {
    await knex(TABLE_NAME).where({ postId }).delete();
  } catch (error) {
    throw databaseError(error);
  }
}
