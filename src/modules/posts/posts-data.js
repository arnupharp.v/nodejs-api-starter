import { databaseError } from '../../helpers/error';
import { getCurrentTime } from '../../helpers/datetime';
import { getFieldName, getLikeOperator, knexQueryRangeWithAlias } from '../../helpers/knex-query';
import { STATUS_DELETED } from '../../helpers/constant';
import knex from '../../connections/knex';

const postsAlias = {
  name: 'posts_alias',
  query: `SELECT p.*, c.name AS categoryName
  FROM posts p 
      LEFT JOIN categories c on c.id = ${getFieldName('p.categoryId')}
  WHERE p.status <> ? `,
  params: [STATUS_DELETED],
};

const TABLE_NAME = 'posts';

export async function getPostById(id) {
  try {
    const conBuilders = [];
    conBuilders.push((builder) => {
      builder.where({ id });
    });

    const result = await knexQueryRangeWithAlias(postsAlias, conBuilders, {
      offset: 0,
      limit: 1,
      sorts: [],
    });
    return result.data.length > 0 ? result.data[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createPost(post, createdBy) {
  try {
    const saveData = {
      ...post,
      createdAt: getCurrentTime(),
      createdBy,
      updatedAt: getCurrentTime(),
      updatedBy: createdBy,
    };

    return knex(TABLE_NAME)
      .insert(saveData)
      .then((ids) => {
        const id = ids.length ? ids[0] : 0;
        return getPostById(id);
      });
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updatePost(id, post, updatedBy) {
  try {
    const saveData = {
      ...post,
      updatedAt: getCurrentTime(),
      updatedBy,
    };

    await knex(TABLE_NAME).where({ id }).update(saveData);
    return getPostById(id);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getAllPosts({ offset, limit, filter, sorts }) {
  try {
    const conBuilders = [];

    if (filter) {
      const { keyword, postType, status, categoryIds, tagIds, publishedAt, expiredAt } = filter;

      conBuilders.push((builder) => {
        builder.whereNot('status', STATUS_DELETED);
      });

      if (status) {
        conBuilders.push((builder) => {
          builder.where('status', status);
        });
      }

      if (postType) {
        conBuilders.push((builder) => {
          builder.where('postType', postType);
        });
      }

      if (categoryIds && categoryIds.length) {
        conBuilders.push((builder) => {
          builder.whereIn('categoryId', categoryIds);
        });
      }

      if (publishedAt) {
        conBuilders.push((builder) => {
          builder.where('publishedAt', null).orWhere('publishedAt', '<=', publishedAt);
        });
      }

      if (expiredAt) {
        conBuilders.push((builder) => {
          builder.where('expiredAt', null).orWhere('expiredAt', '>=', expiredAt);
        });
      }

      const likeOp = getLikeOperator();
      if (keyword) {
        conBuilders.push((builder) => {
          builder
            .where('title', likeOp, `%${keyword}%`)
            .orWhere('description', likeOp, `%${keyword}%`)
            .orWhereRaw(`REPLACE(CAST("keywords" as text), '"', '') ${likeOp} ?`, [`%${keyword}%`])
            .orWhereRaw(`REPLACE(CAST("tagNames" as text), '"', '') ${likeOp} ?`, [`%${keyword}%`]);
        });
      }

      if (tagIds && tagIds.length) {
        let tagIdEntries = '';
        tagIds.forEach((tagId) => {
          if (tagIdEntries) tagIdEntries += ', ';
          tagIdEntries += `${tagId}`;
        });

        conBuilders.push((builder) => {
          builder.whereRaw(
            `EXISTS (SELECT null 
                      FROM post_tags pt 
                      WHERE ${getFieldName('pt.postId')} = posts_alias.id AND ${getFieldName(
              'pt.tagId',
            )} IN (${tagIdEntries}))	`,
          );
        });
      }
    }

    return knexQueryRangeWithAlias(postsAlias, conBuilders, { offset, limit, sorts });
  } catch (error) {
    throw databaseError(error);
  }
}
