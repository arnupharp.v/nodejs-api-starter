import { createPost, updatePost, getAllPosts, getPostById } from './posts-data';
import { notFound } from '../../helpers/error';
import {
  POST_STATUS_DRAFT,
  PAGINATION_DEFAULT_LIMIT,
  STATUS_DELETED,
} from '../../helpers/constant';
import {
  createTagsService,
  getTagsByNamesService,
  getTagsByTagStringService,
  getTagSlugFromName,
} from '../tags/tags-service';
import { deserializeJsonbValue, serializeJsonbValue } from '../../helpers/database';
import { clearPostTagsById, createPostTags } from './posts-tags-data';

export function deserializePostFields(post) {
  if (!post) return null;

  return {
    ...post,
    context: deserializeJsonbValue(post.context),
    tagNames: deserializeJsonbValue(post.tagNames),
    keywords: deserializeJsonbValue(post.keywords),
  };
}

export async function getPostByIdService(id) {
  const data = await getPostById(id);
  return deserializePostFields(data);
}

export async function applyPostTagsService(postId, tagNames) {
  await clearPostTagsById(postId);

  if (!tagNames || !tagNames.length) return;

  const newTags = [];
  let tagObjects = await getTagsByNamesService(tagNames);
  tagNames.forEach((name) => {
    const slug = getTagSlugFromName(name);
    const existing = tagObjects.find((o) => o.slug === slug);
    if (!existing) {
      newTags.push(name);
    }
  });

  if (newTags.length > 0) {
    await createTagsService(newTags, 'post');

    // Reload tagObjects again
    tagObjects = await getTagsByNamesService(tagNames);
  }

  const postTags = [];
  for (let i = 0; i < tagObjects.length; i += 1) {
    postTags.push({
      postId,
      tagId: tagObjects[i].id,
      ordering: i + 1,
    });
  }

  await createPostTags(postTags);
}

export async function createPostService(
  postType,
  title,
  description,
  content,
  status,
  publishedAt,
  expiredAt,
  context,
  keywords,
  tagNames,
  imageUrl,
  url,
  categoryId,
  createdBy,
) {
  const saveData = {
    postType,
    title,
    description,
    content,
    status: status || POST_STATUS_DRAFT,
    publishedAt,
    expiredAt,
    context: JSON.stringify(context || {}),
    tagNames: serializeJsonbValue(tagNames),
    keywords: serializeJsonbValue(keywords),
    imageUrl,
    url,
    categoryId,
  };

  const post = await createPost(saveData, createdBy);
  await applyPostTagsService(post.id, tagNames || []);

  return deserializePostFields(post);
}

export async function updatePostService(
  id,
  postType,
  title,
  description,
  content,
  status,
  publishedAt,
  expiredAt,
  context,
  tagNames,
  imageUrl,
  url,
  categoryId,
  updatedBy,
) {
  const existing = await getPostById(id);
  if (!existing) throw notFound();

  const saveData = {
    postType,
    title,
    description,
    content,
    status: status || POST_STATUS_DRAFT,
    publishedAt,
    expiredAt,
    context: JSON.stringify(context || {}),
    tagNames: JSON.stringify(tagNames || []),
    imageUrl,
    url,
    categoryId,
  };

  await updatePost(id, saveData, updatedBy);
  await applyPostTagsService(id, tagNames || []);

  const updated = await getPostByIdService(id);
  return deserializePostFields(updated);
}

export async function deletePostService(id, deletedBy) {
  const existing = await getPostById(id);
  if (!existing) throw notFound();

  await updatePost(id, { status: STATUS_DELETED }, deletedBy);
  return true;
}

export async function getAllPostsService({
  offset,
  limit,
  filter,
  sortBy = 'title',
  sortDirection = 'asc',
}) {
  const opt = {
    offset,
    limit,
    filter,
    sorts: [{ column: sortBy, direction: sortDirection }],
  };

  if (!opt.offset) opt.offset = 0;
  if (!opt.limit) opt.limit = PAGINATION_DEFAULT_LIMIT;

  if (opt.filter && opt.filter.tags) {
    // Convert tags to list of tag id
    const tagObjects = await getTagsByTagStringService(opt.filter.tags);
    if (tagObjects.length) {
      const tagIds = [];
      tagObjects.forEach((t) => {
        tagIds.push(t.id);
      });

      opt.filter.tagIds = tagIds;
    }
  }

  if (opt.filter && opt.filter.categories) {
    // Convert list of category id
    const idList = opt.filter.categories.trim().split(';');
    if (idList.length) {
      const categoryIds = [];
      idList.forEach((t) => {
        categoryIds.push(parseInt(t, 10));
      });

      opt.filter.categoryIds = categoryIds;
    }
  }

  const result = await getAllPosts(opt);
  for (let i = 0; i < result.data.length; i += 1) {
    result.data[i] = deserializePostFields(result.data[i]);
  }

  return result;
}
