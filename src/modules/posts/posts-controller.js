import { body } from 'express-validator';
import createHandler from '../../helpers/async-controller';
import { validate } from '../../helpers/validator';
import { send } from '../../helpers/response';
import {
  createPostService,
  updatePostService,
  deletePostService,
  getPostByIdService,
  getAllPostsService,
} from './posts-service';
import { getCurrentTime } from '../../helpers/datetime';
import { isForBackOffice } from '../../helpers/request';
import { STATUS_PUBLISHED } from '../../helpers/constant';
import { checkPermission, checkPermissionByCredential } from '../../helpers/auth';
import { notFound } from '../../helpers/error';

export const createPostController = {
  checkPermission: checkPermission(['posts.*.create']),
  validator: validate([
    body('title').notEmpty().withMessage('is required'),
    body('postType').notEmpty().withMessage('is required'),
    body('status').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      keywords,
      tagNames,
      imageUrl,
      url,
      categoryId,
    } = req.body;
    const { credential } = req;
    const created = await createPostService(
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      keywords,
      tagNames,
      imageUrl,
      url,
      categoryId,
      credential.id,
    );
    send({ code: 200, data: created }, req, res);
  }),
};

export const updatePostController = {
  checkPermission: checkPermission(['posts.*.update']),
  validator: validate([
    body('title').notEmpty().withMessage('is required'),
    body('postType').notEmpty().withMessage('is required'),
    body('status').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      imageUrl,
      url,
      categoryId,
    } = req.body;
    const { credential } = req;
    const updated = await updatePostService(
      req.params.id,
      postType,
      title,
      description,
      content,
      status,
      publishedAt,
      expiredAt,
      context,
      tagNames,
      imageUrl,
      url,
      categoryId,
      credential.email,
    );
    send({ code: 200, data: updated }, req, res);
  }),
};

export const deletePostController = {
  checkPermission: checkPermission(['posts.*.delete']),
  handler: createHandler(async (req, res) => {
    const deleted = await deletePostService(req.params.id);
    send({ code: 200, data: deleted }, req, res);
  }),
};

export const getPostByIdController = {
  checkPermission: checkPermission(['posts.*.read']),
  handler: createHandler(async (req, res) => {
    const post = await getPostByIdService(req.params.id);
    if (post) {
      if (
        post.status !== STATUS_PUBLISHED &&
        !(await checkPermissionByCredential(req.credential, ['posts.*.read']))
      ) {
        throw notFound();
      }

      send({ code: 200, data: post }, req, res);
    } else {
      send({ code: 404 }, req, res);
    }
  }),
};

export const getAllPostsController = {
  checkPermission: checkPermission(['posts.*.list']),
  handler: createHandler(async (req, res) => {
    const { keyword, offset, limit, postType, categories, tags, status, sortBy, sortDirection } =
      req.query;
    const filter = {
      keyword,
      postType,
      categories,
      tags,
    };

    if (!isForBackOffice(req)) {
      // This request is come from frontend, we need to return published post only.
      filter.status = STATUS_PUBLISHED;
      filter.publishedAt = getCurrentTime();
      filter.expiredAt = getCurrentTime();
    } else if (status) {
      filter.status = status;
    }

    const result = await getAllPostsService({
      offset,
      limit,
      filter,
      sortBy,
      sortDirection,
    });

    if (result) {
      send({ code: 200, data: result.data, meta: result.meta }, req, res);
    } else {
      send({ code: 400 }, req, res);
    }
  }),
};
