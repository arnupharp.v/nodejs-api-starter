import express from 'express';
import {
  createPostController,
  updatePostController,
  deletePostController,
  getPostByIdController,
  getAllPostsController,
} from './posts-controller';

const router = express.Router();

router.get('/', getAllPostsController.checkPermission, getAllPostsController.handler);
router.get('/:id', getPostByIdController.checkPermission, getPostByIdController.handler);
router.post(
  '/',
  createPostController.checkPermission,
  createPostController.validator,
  createPostController.handler,
);
router.patch(
  '/:id',
  updatePostController.checkPermission,
  updatePostController.validator,
  updatePostController.handler,
);
router.delete('/:id', deletePostController.checkPermission, deletePostController.handler);

export default router;
