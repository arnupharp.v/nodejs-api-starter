import knex from '../../connections/knex';
import { databaseError } from '../../helpers/error';

const TABLE_NAME = 'tags';

export async function getTagById(id) {
  try {
    const tags = await knex(TABLE_NAME).where({ id });
    return tags.length > 0 ? tags[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createTag(tag) {
  try {
    return knex(TABLE_NAME)
      .insert(tag)
      .then((ids) => {
        const id = ids.length ? ids[0] : 0;
        return getTagById(id);
      });
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createTags(tags) {
  try {
    const result = await knex(TABLE_NAME).insert(tags);
    return result;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateTag(id, tag) {
  try {
    await knex(TABLE_NAME).where({ id }).update(tag);
    return getTagById(id);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getTagsByIds(tagIds) {
  try {
    const tags = await knex(TABLE_NAME).whereIn('id', tagIds);
    return tags;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getTagsBySlugs(slugs) {
  try {
    const tags = await knex(TABLE_NAME).whereIn('slug', slugs);
    return tags;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getTagBySlug(slug) {
  try {
    const tags = await knex(TABLE_NAME).where({ slug });

    return tags.length > 0 ? tags[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}
