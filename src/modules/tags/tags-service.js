import * as TagData from './tags-data';
import { stringToSlug } from '../../helpers/string';

export function getTagSlugFromName(tagName) {
  const slug = stringToSlug(tagName || '');
  return slug;
}

export async function getTagByIdService(id) {
  const data = await TagData.getTagById(id);
  return data;
}

export async function getTagsByIdsService(tagIds) {
  const data = await TagData.getTagsByIds(tagIds);
  return data;
}

export async function getTagByNameService(name) {
  const slug = getTagSlugFromName(name);
  const data = await TagData.getTagBySlug(slug);
  return data;
}

export async function getTagsByNamesService(names) {
  const slugs = [];
  names.forEach((name) => {
    slugs.push(getTagSlugFromName(name));
  });

  const data = await TagData.getTagsBySlugs(slugs);
  return data;
}

export async function getTagsByTagStringService(tagString) {
  const tagList = [];
  (tagString || '')
    .trim()
    .toLowerCase()
    .split(';')
    .forEach((tag) => {
      const t = tag.trim();
      if (t) tagList.push(t);
    });

  if (tagList.length) {
    const tagObjects = await getTagsByNamesService(tagList);
    return tagObjects;
  }

  return [];
}

export async function createTagService(name) {
  const slug = getTagSlugFromName(name);
  const saveData = {
    name,
    slug,
  };
  console.log('creating tag:', saveData);
  const data = await TagData.createTag(saveData);
  return data;
}

export async function createTagsService(names) {
  const saveData = [];
  names.forEach((name) => {
    const slug = getTagSlugFromName(name);
    saveData.push({
      name,
      slug,
    });
  });

  const data = await TagData.createTags(saveData);
  return data;
}
