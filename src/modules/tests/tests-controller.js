import createHandler from '../../helpers/async-controller';
import { v4 as uuidv4 } from 'uuid';
import { send } from '../../helpers/response';

export const getUuidController = {
  handler: createHandler(async (req, res) => {
    const uuids = [];
    const total = req.query.total || 1;
    for (let i = 0; i < total; i += 1) {
      uuids.push(uuidv4());
    }

    send({ code: 200, data: uuids }, req, res);
  }),
};
