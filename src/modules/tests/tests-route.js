import express from 'express';
import { getUuidController } from './tests-controller';

const router = express.Router();

router.get('/uuid', getUuidController.handler);

export default router;
