import { databaseError } from '../../helpers/error';
import knex from '../../connections/knex';
import { getCurrentTime } from '../../helpers/datetime';

export async function clearUserConsents(userId, consentFormId) {
  try {
    const consentForms = await knex('user_consents').where({ userId, consentFormId }).del();

    return consentForms.length > 0 ? consentForms[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function addUserConsents(userConsents) {
  try {
    await knex('user_consents').insert(userConsents);
    return true;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getUserConsentsByChecksum(userId, consentFormChecksum) {
  try {
    const consents = await knex('user_consents').where({ userId, consentFormChecksum });
    return consents;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateUserConsentById(
  id,
  { userId, consentFormId, consentFormChecksum, consentItemKey, isConsented, expiredAt, updatedBy },
) {
  try {
    const now = getCurrentTime();
    const data = {
      userId,
      consentFormId,
      consentFormChecksum,
      consentItemKey,
      isConsented,
      expiredAt,
      updatedAt: now,
      updatedBy,
    };

    await knex('user_consents').where({ id }).update(data);
    return true;
  } catch (error) {
    throw databaseError(error);
  }
}
