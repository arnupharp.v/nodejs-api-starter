import { databaseError } from '../../helpers/error';
import { STATUS_ACTIVE } from '../../helpers/constant';
import knex from '../../connections/knex';

export async function getActiveConsentForm() {
  try {
    const consentForms = await knex('consent_forms').where({ status: STATUS_ACTIVE });

    return consentForms.length > 0 ? consentForms[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getConsentFormByChecksum(consentFormChecksum) {
  try {
    const consentForms = await knex('consent_forms').where({ consentFormChecksum });

    return consentForms.length > 0 ? consentForms[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}
