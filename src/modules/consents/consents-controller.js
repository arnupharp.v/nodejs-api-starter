import createHandler from '../../helpers/async-controller';
import { send } from '../../helpers/response';
import { getActiveConsentFormService } from './consents-form-service';

export const getActiveConsentFormController = {
  handler: createHandler(async (req, res) => {
    const form = await getActiveConsentFormService();
    if (form) {
      send({ code: 200, data: form }, req, res);
    } else {
      send({ code: 404 }, req, res);
    }
  }),
};
