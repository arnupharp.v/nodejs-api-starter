import {
  addUserConsents,
  clearUserConsents,
  updateUserConsentById,
  getUserConsentsByChecksum,
} from './consents-user-data';
import { getCurrentTime, addDays, formatForDB } from '../../helpers/datetime';
import { deserializeJsonbValue } from '../../helpers/database';

export async function applyUserConsentsService({ userId, consentForm, consentedKeys }) {
  const userConsents = [];
  const { consentFormChecksum, context } = consentForm;
  const { consentItems } = deserializeJsonbValue(context);

  const currentDate = getCurrentTime();
  const expiredDate = addDays(process.env.APP_CONSENT_EXPIRE_DAYS);

  consentItems.forEach((item) => {
    userConsents.push({
      userId,
      consentFormId: consentForm.id,
      consentFormChecksum,
      consentItemKey: item.key,
      isConsented: consentedKeys.includes(item.key),
      expiredAt: formatForDB(expiredDate),
      createdAt: currentDate,
      createdBy: userId,
      updatedAt: currentDate,
      updatedBy: userId,
    });
  });

  await clearUserConsents(userId, consentForm.id);
  await addUserConsents(userConsents);

  return getUserConsentsByChecksum(userId, consentFormChecksum);
}

export async function extendUserConsentsExpiryDateService(userId, consentFormChecksum) {
  const days = process.env.APP_CONSENT_EXPIRE_DAYS;
  const consents = await getUserConsentsByChecksum(userId, consentFormChecksum);
  for (let i = 0; i < consents.length; i += 1) {
    const consent = consents[i];

    // eslint-disable-next-line
    await updateUserConsentById(consent.id, {
      expiredAt: addDays(days),
      updatedBy: userId,
    });
  }

  return getUserConsentsByChecksum(userId, consentFormChecksum);
}

export async function getUserConsentedKeysService(userId, consentFormChecksum) {
  const consents = await getUserConsentsByChecksum(userId, consentFormChecksum);
  const currentDate = new Date();
  const consentedKeys = [];
  for (let i = 0; i < consents.length; i += 1) {
    if (consents[i].isConsented && consents[i].expiredAt >= currentDate)
      consentedKeys.push(consents[i].consentItemKey);
  }

  return consentedKeys;
}
