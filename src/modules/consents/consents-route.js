import express from 'express';
import { getActiveConsentFormController } from './consents-controller';

const router = express.Router();

router.get('/active-form', getActiveConsentFormController.handler);

export default router;
