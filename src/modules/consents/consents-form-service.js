import { getActiveConsentForm, getConsentFormByChecksum } from './consents-form-data';
import { deserializeJsonbValue } from '../../helpers/database';

export async function getActiveConsentFormService() {
  const consentForm = await getActiveConsentForm();
  if (consentForm) consentForm.context = deserializeJsonbValue(consentForm.context);

  return consentForm;
}

export async function getConsentFormByChecksumService(checksum) {
  return getConsentFormByChecksum(checksum);
}
