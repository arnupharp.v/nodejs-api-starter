import knex from '../../connections/knex';
import random from 'randomstring';
import { v4 as uuidv4 } from 'uuid';
import { AUTH_STATUS_VERIFIED, STATUS_DELETED } from '../../helpers/constant';
import { createPasswordHash } from './users-domain';
import { addDays, getCurrentTime } from '../../helpers/datetime';
import { databaseError } from '../../helpers/error';
import moment from 'moment';
import { knexQueryRange } from '../../helpers/knex-query';

const TABLE_NAME = 'users';

export async function getUserById(id) {
  try {
    const users = await knex(TABLE_NAME).where({ id });

    return users.length > 0 ? users[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createLocalUser({
  email,
  password,
  gender,
  firstName,
  lastName,
  telephone,
  birthDate,
  consentFormChecksum,
  status,
  createdBy,
}) {
  try {
    const now = getCurrentTime();
    const id = uuidv4();
    const user = {
      id,
      gender,
      firstName,
      lastName,
      email,
      telephone,
      birthDate,
      consentFormChecksum,
      provider: 'local',
      passwordHash: createPasswordHash(password),
      refreshTokenId: random.generate(50),
      refreshTokenSecret: random.generate(20),
      resetPasswordToken: status === AUTH_STATUS_VERIFIED ? null : random.generate(50),
      status,
      verifiedAt: status === AUTH_STATUS_VERIFIED ? now : null,
      createdBy: createdBy || id,
      createdAt: now,
      updatedBy: createdBy || id,
      updatedAt: now,
    };
    await knex(TABLE_NAME).insert(user);

    return getUserById(id);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createThirdPartyUser({
  id,
  email,
  gender,
  firstName,
  lastName,
  telephone,
  birthDate,
  consentFormChecksum,
  provider,
}) {
  try {
    const now = getCurrentTime();
    const userId = uuidv4();
    const user = {
      id: userId,
      gender,
      firstName,
      lastName,
      telephone,
      birthDate,
      email,
      consentFormChecksum,
      provider,
      providerId: id,
      refreshTokenId: random.generate(50),
      refreshTokenSecret: random.generate(20),
      status: AUTH_STATUS_VERIFIED,
      verifiedAt: now,
      createdBy: userId,
      createdAt: now,
      updatedBy: userId,
      updatedAt: now,
    };
    await knex(TABLE_NAME).insert(user);
    return getUserById(userId);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateLocalUserByEmail(email, data) {
  try {
    const updated = await knex(TABLE_NAME)
      .where({ provider: 'local', email })
      .update(data)
      .returning('*');
    return updated[0];
  } catch (error) {
    throw databaseError(error);
  }
}

export async function updateUserById(id, data) {
  try {
    await knex(TABLE_NAME).where({ id }).update(data);
    return getUserById(id);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getLocalUserByResetPasswordToken(resetPasswordToken) {
  const users = await knex(TABLE_NAME).where({ provider: 'local', resetPasswordToken });
  return users[0];
}

export async function getLocalUserByEmail(email) {
  const users = await knex(TABLE_NAME).where({ provider: 'local', email });
  return users[0];
}

export async function getUserByProviderId(provider, providerId) {
  const users = await knex(TABLE_NAME).where({ provider, providerId });
  return users[0];
}

export async function getUserByRefreshTokenId(refreshTokenId) {
  const users = await knex(TABLE_NAME).where({ refreshTokenId });
  return users[0];
}

export async function countLocalUserByEmail(email) {
  try {
    const result = await knex(TABLE_NAME).count('*').where({ provider: 'local', email });
    return Number(result[0].count);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function createFbUser({ id, email, firstName, lastName, tel }) {
  try {
    const now = getCurrentTime();
    const user = {
      id: random.generate({
        length: 13,
        charset: 'numeric',
      }),
      firstName,
      lastName,
      email,
      tel,
      provider: 'fb',
      providerId: id,
      refreshTokenId: random.generate(50),
      refreshTokenSecret: random.generate(20),
      status: AUTH_STATUS_VERIFIED,
      verifiedAt: now,
      createdAt: now,
      updatedAt: now,
    };
    const created = await knex(TABLE_NAME).insert(user).returning('*');
    return created[0];
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getLocalUserById(id) {
  const users = await knex(TABLE_NAME).select('*').where({ provider: 'local', id });
  return users[0];
}

export async function getUserByEmail(email) {
  try {
    const users = await knex(TABLE_NAME).where({ email }).whereNot('status', STATUS_DELETED);

    return users.length > 0 ? users[0] : null;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getUsersByEmails(emails) {
  try {
    const users = await knex(TABLE_NAME).whereIn('email', emails);
    return users;
  } catch (error) {
    throw databaseError(error);
  }
}

export async function getAllUsers({ offset, limit, filter, sorts }) {
  try {
    const conBuilders = [];

    if (filter) {
      const { keyword, status, verifiedAtFrom, verifiedAtTo } = filter;
      if (status) {
        conBuilders.push((builder) => {
          builder.where('status', status);
        });
      }

      if (verifiedAtFrom) {
        conBuilders.push((builder) => {
          builder.where('verifiedAt', '>=', verifiedAtFrom);
        });
      }

      if (verifiedAtTo) {
        conBuilders.push((builder) => {
          const toDate = addDays(1, moment(verifiedAtTo).toDate());
          builder.where('verifiedAt', '<', moment(toDate).format('YYYY-MM-DD'));
        });
      }

      if (keyword) {
        conBuilders.push((builder) => {
          builder
            .where('firstName', 'like', `%${keyword}%`)
            .orWhere('lastName', 'like', `%${keyword}%`)
            .orWhere('email', 'like', `%${keyword}%`);
        });
      }
    }

    return knexQueryRange(TABLE_NAME, conBuilders, { offset, limit, sorts });
  } catch (error) {
    throw databaseError(error);
  }
}
