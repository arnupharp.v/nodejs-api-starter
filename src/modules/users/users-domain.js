import bcrypt from 'bcrypt';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import { base64Encode, base64Decode } from '../../helpers/string';
import { invalidToken } from '../../helpers/error';

export function createPasswordHash(password) {
  const salt = bcrypt.genSaltSync(10);
  const passwordHash = bcrypt.hashSync(password, salt);
  return passwordHash;
}

export function isPasswordMatchHash(inputPassword, hashPassword) {
  return new Promise((resolve) => {
    bcrypt.compare(inputPassword, hashPassword, (error, result) => {
      if (error || !result) {
        resolve(false);
      }
      resolve(true);
    });
  });
}

export function createRefreshTokenSignature(refreshTokenId, refreshTokenSecret, passwordHash) {
  const algorithm = 'sha256';

  // signature = hmachex(`${refreshTokenId}:${passwordHash}:${refreshTokenSecret}`, refreshTokenSecret)
  const signature = crypto
    .createHmac(algorithm, `${refreshTokenSecret}`)
    .update(`${refreshTokenId}:${passwordHash}:${refreshTokenSecret}`)
    .digest('hex');

  return signature;
}

export function createRefreshToken(refreshTokenId, refreshTokenSecret, passwordHash) {
  const signature = createRefreshTokenSignature(refreshTokenId, refreshTokenSecret, passwordHash);

  // refreshToken = base64(`${refreshTokenId}:${signature}`)
  const refreshToken = base64Encode(`${refreshTokenId}:${signature}`);

  return refreshToken;
}

export function signAccessToken(accessInfo) {
  const accessToken = jwt.sign(accessInfo, `${process.env.JWT_SECRET}`, {
    expiresIn: parseInt(process.env.JWT_EXPIRE, 10),
  });
  return accessToken;
}

export async function createAccessToken(id, email, roleIds) {
  const accessToken = signAccessToken({ id, email, roleIds });
  return accessToken;
}

export function removeSensitiveFields(user) {
  const {
    refreshTokenId,
    refreshTokenSecret,
    passwordHash,
    resetPasswordToken,
    ...nonSensitive
  } = user;
  return nonSensitive;
}

export function decodeRefreshToken(refreshToken) {
  const decoded = base64Decode(refreshToken);
  const token = decoded.split(':');
  if (token.length !== 2) {
    throw invalidToken();
  }
  return { refreshTokenId: token[0], signature: token[1] };
}

export function isRefreshTokenValid(
  signatureFromToken,
  refreshTokenId,
  refreshTokenSecret,
  passwordHash,
) {
  const signature = createRefreshTokenSignature(refreshTokenId, refreshTokenSecret, passwordHash);
  return signatureFromToken === signature;
}

export function removeSensitiveProperties(user) {
  /* eslint-disable no-param-reassign */
  delete user.passwordHash;
  delete user.refreshTokenId;
  delete user.refreshTokenSecret;
  /* eslint-enable no-param-reassign */

  return user;
}
