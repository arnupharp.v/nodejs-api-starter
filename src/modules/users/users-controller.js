import { body, cookie } from 'express-validator';
import createHandler from '../../helpers/async-controller';
import { validate } from '../../helpers/validator';
import {
  getUserByIdService,
  createLocalUserService,
  updateProfileService,
  signinService,
  verifyLocalUserService,
  renewAccessTokenService,
  facebookSigninService,
  createResetPasswordLinkService,
  resetPasswordService,
  updatePasswordService,
  getAllUsersService,
  updateUserByIdService,
  googleSigninService,
  deleteUserByIdService,
} from './users-service';
import { send, setCookie } from '../../helpers/response';
import { notFound } from '../../helpers/error';
import { removeSensitiveFields } from './users-domain';
import { applyUserConsentsService } from '../consents/consents-user-service';
import { getConsentFormByChecksumService } from '../consents/consents-form-service';
import { AUTH_STATUS_VERIFIED } from '../../helpers/constant';
import { checkPermission, requiredAuth } from '../../helpers/auth';

export const getUserController = {
  checkPermission: requiredAuth(),
  handler: createHandler(async (req, res) => {
    const user = await getUserByIdService(req.credential.id);
    if (!user) {
      throw notFound();
    }
    send({ code: 200, data: { ...removeSensitiveFields(user) } }, req, res);
  }),
};

export const createLocalUserController = {
  validator: validate([
    body('email').isEmail().withMessage('is not a valid email'),
    body('password').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { email, password, firstName, lastName, telephone, consentedKeys, consentFormChecksum } =
      req.body;
    const created = await createLocalUserService({
      email,
      password,
      firstName,
      lastName,
      telephone,
      consentedKeys,
      consentFormChecksum,
    });

    const { user, accessToken, refreshToken } = created;

    if (user.status === AUTH_STATUS_VERIFIED) {
      setCookie(
        'refreshToken',
        refreshToken,
        {
          httpOnly: true,
          secure: process.env.NODE_ENV !== 'development',
          sameSite: process.env.NODE_ENV !== 'development' ? 'none' : undefined,
        },
        res,
      );

      send({ code: 200, data: { ...user, accessToken } }, req, res);
    } else {
      send({ code: 200, data: user }, req, res);
    }
  }),
};

// This action should be called in backoffice
export const createUserController = {
  checkPermission: checkPermission(['users.*.create']),
  validator: validate([
    body('email').isEmail().withMessage('is not a valid email'),
    body('password').notEmpty().withMessage('is required'),
    body('firstName').notEmpty().withMessage('is required'),
    body('lastName').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      email,
      password,
      firstName,
      lastName,
      telephone,
      consentedKeys,
      consentFormChecksum,
      roleIds,
    } = req.body;

    const created = await createLocalUserService(
      {
        email,
        password,
        firstName,
        lastName,
        telephone,
        consentedKeys,
        consentFormChecksum,
        roleIds,
      },
      true,
    );

    const { user } = created;

    send({ code: 200, data: user }, req, res);
  }),
};

export const updateProfileController = {
  checkPermission: requiredAuth(),
  validator: validate([
    body('email').isEmail().withMessage('is not a valid email'),
    body('firstName').notEmpty().withMessage('is required'),
    body('lastName').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const {
      email,
      gender,
      firstName,
      lastName,
      telephone,
      birthDate,
      consentFormChecksum,
      consentedKeys,
    } = req.body;

    let updated = await updateProfileService({
      id: req.credential.id,
      gender,
      email,
      firstName,
      lastName,
      telephone,
      birthDate,
    });

    if (consentFormChecksum) {
      const consentForm = await getConsentFormByChecksumService(consentFormChecksum);
      if (consentForm) {
        await applyUserConsentsService({
          userId: req.credential.id,
          consentedKeys,
          consentForm,
        });

        // Reload user with new consented keys
        updated = await getUserByIdService(req.credential.id);
      }
    }

    send({ code: 200, data: updated }, req, res);
  }),
};

export const updateUserConsentController = {
  checkPermission: requiredAuth(),
  validator: validate([
    body('consentedKeys').notEmpty().withMessage('is required'),
    body('consentFormChecksum').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { consentedKeys, consentFormChecksum } = req.body;

    const consentForm = await getConsentFormByChecksumService(consentFormChecksum);
    if (!consentForm) throw notFound();

    const updated = await applyUserConsentsService({
      userId: req.credential.id,
      consentedKeys,
      consentForm,
    });

    send({ code: 200, data: updated }, req, res);
  }),
};

export const signinController = {
  validator: validate([
    body('email').isEmail().withMessage('is not a valid email'),
    body('password').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { email, password } = req.body;
    const { user, accessToken, refreshToken } = await signinService({ email, password });

    setCookie(
      'refreshToken',
      refreshToken,
      {
        httpOnly: true,
        secure: process.env.NODE_ENV !== 'development',
        sameSite: process.env.NODE_ENV !== 'development' ? 'none' : undefined,
      },
      res,
    );

    send({ code: 200, data: { ...user, accessToken } }, req, res);
  }),
};

export const signoutController = {
  handler: createHandler(async (req, res) => {
    res.clearCookie('refreshToken');
    send({ code: 200, data: true }, req, res);
  }),
};

export const verifyLocalUserController = {
  validator: validate([body('verifyToken').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { verifyToken } = req.body;
    const { user } = await verifyLocalUserService({ verifyToken });

    send({ code: 200, data: { ...user } }, req, res);
  }),
};

export const renewTokenController = {
  validator: validate([cookie('refreshToken').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { refreshToken } = req.cookies;
    const accessToken = await renewAccessTokenService(refreshToken);
    const data = { accessToken };
    send({ code: 200, data }, req, res);
  }),
};

export const createResetPasswordLinkController = {
  validator: validate([body('email').isEmail().withMessage('is not a valid email')]),
  handler: createHandler(async (req, res) => {
    const { email } = req.body;
    const result = await createResetPasswordLinkService(email);
    send({ code: 200, data: result }, req, res);
  }),
};

export const facebookSigninController = {
  validator: validate([body('fbAccessToken').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { fbAccessToken } = req.body;
    const { user, accessToken, refreshToken } = await facebookSigninService({ fbAccessToken });

    setCookie(
      'refreshToken',
      refreshToken,
      {
        httpOnly: true,
        secure: process.env.NODE_ENV !== 'development',
        sameSite: process.env.NODE_ENV !== 'development' ? 'none' : undefined,
      },
      res,
    );

    send({ code: 200, data: { ...user, accessToken } }, req, res);
  }),
};

export const googleSigninController = {
  validator: validate([body('idToken').notEmpty().withMessage('is required')]),
  handler: createHandler(async (req, res) => {
    const { idToken } = req.body;
    const { user, accessToken, refreshToken } = await googleSigninService({ idToken });

    setCookie(
      'refreshToken',
      refreshToken,
      {
        httpOnly: true,
        secure: process.env.NODE_ENV !== 'development',
        sameSite: process.env.NODE_ENV !== 'development' ? 'none' : undefined,
      },
      res,
    );

    send({ code: 200, data: { ...user, accessToken } }, req, res);
  }),
};

export const resetPasswordController = {
  validator: validate([
    body('password').notEmpty().withMessage('is required'),
    body('resetToken').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { password, resetToken } = req.body;
    const result = await resetPasswordService(password, resetToken);
    send({ code: 200, data: result }, req, res);
  }),
};

export const updatePasswordController = {
  checkPermission: requiredAuth(),
  validator: validate([
    body('oldPassword').notEmpty().withMessage('is required'),
    body('newPassword').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { credential } = req;
    const { oldPassword, newPassword } = req.body;
    const result = await updatePasswordService(credential.id, oldPassword, newPassword);
    send({ code: 200, data: result }, req, res);
  }),
};

export const getAllUsersController = {
  checkPermission: checkPermission(['users.*.list']),
  handler: createHandler(async (req, res) => {
    const {
      keyword,
      isAdmin,
      offset,
      limit,
      status,
      sortBy,
      sortDirection,
      verifiedAtFrom,
      verifiedAtTo,
    } = req.query;
    const filter = {
      keyword,
      status,
      isAdmin,
      verifiedAtFrom,
      verifiedAtTo,
    };

    const result = await getAllUsersService({
      offset,
      limit,
      filter,
      sortBy,
      sortDirection,
    });

    if (result) {
      send({ code: 200, data: result.data, meta: result.meta }, req, res);
    } else {
      send({ code: 400 }, req, res);
    }
  }),
};

export const getUserByIdController = {
  checkPermission: checkPermission(['users.*.read']),
  handler: createHandler(async (req, res) => {
    const { id } = req.params;

    const user = await getUserByIdService(id, true, true);
    if (!user) throw notFound();

    send({ code: 200, data: removeSensitiveFields(user) }, req, res);
  }),
};

export const updateUserByIdController = {
  checkPermission: checkPermission(['users.*.update']),
  validator: validate([
    body('email').isEmail().withMessage('is not a valid email'),
    body('firstName').notEmpty().withMessage('is required'),
    body('lastName').notEmpty().withMessage('is required'),
    body('status').notEmpty().withMessage('is required'),
  ]),
  handler: createHandler(async (req, res) => {
    const { id } = req.params;
    const { email, gender, firstName, lastName, telephone, birthDate, status, roleIds } = req.body;

    const user = await getUserByIdService(id);
    if (!user) throw notFound();

    const updated = await updateUserByIdService(user.id, {
      email,
      gender,
      firstName,
      lastName,
      telephone,
      birthDate,
      status,
      roleIds,
      updatedBy: req.credential.id,
    });

    send({ code: 200, data: updated.user }, req, res);
  }),
};

export const deleteUserByIdController = {
  checkPermission: checkPermission(['users.*.delete']),
  handler: createHandler(async (req, res) => {
    const { id } = req.params;
    const user = await getUserByIdService(id, false);
    if (!user) throw notFound();

    await deleteUserByIdService(id, req.credential.id);
    send({ code: 200, data: true }, req, res);
  }),
};
