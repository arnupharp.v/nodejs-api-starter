import express from 'express';
import {
  createUserController,
  updateUserByIdController,
  deleteUserByIdController,
  getUserByIdController,
  getAllUsersController,
} from './users-controller';

const router = express.Router();

router.get('/', getAllUsersController.checkPermission, getAllUsersController.handler);
router.get('/:id', getUserByIdController.checkPermission, getUserByIdController.handler);
router.post(
  '/',
  createUserController.checkPermission,
  createUserController.validator,
  createUserController.handler,
);
router.patch(
  '/:id',
  updateUserByIdController.checkPermission,
  updateUserByIdController.validator,
  updateUserByIdController.handler,
);
router.delete('/:id', deleteUserByIdController.checkPermission, deleteUserByIdController.handler);

export default router;
