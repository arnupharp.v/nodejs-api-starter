import express from 'express';
import {
  getUserController,
  updateProfileController,
  updateUserConsentController,
  createLocalUserController,
  verifyLocalUserController,
  signinController,
  signoutController,
  renewTokenController,
  createResetPasswordLinkController,
  facebookSigninController,
  googleSigninController,
  resetPasswordController,
  updatePasswordController,
} from './users-controller';

const router = express.Router();

router.get('/me', getUserController.checkPermission, getUserController.handler);
router.patch(
  '/me/consent',
  updateUserConsentController.checkPermission,
  updateUserConsentController.validator,
  updateUserConsentController.handler,
);
router.patch(
  '/me',
  updateProfileController.checkPermission,
  updateProfileController.validator,
  updateProfileController.handler,
);

router.post('/signup', createLocalUserController.validator, createLocalUserController.handler);
router.post('/signin', signinController.validator, signinController.handler);
router.get('/signout', signoutController.handler);
router.post('/verify', verifyLocalUserController.validator, verifyLocalUserController.handler);
router.post('/access-tokens', renewTokenController.validator, renewTokenController.handler);

router.post(
  '/reset-password',
  createResetPasswordLinkController.validator,
  createResetPasswordLinkController.handler,
);
router.patch('/reset-password', resetPasswordController.validator, resetPasswordController.handler);
router.patch(
  '/update-password',
  updatePasswordController.checkPermission,
  updatePasswordController.validator,
  updatePasswordController.handler,
);
router.post(
  '/signin-facebook',
  facebookSigninController.validator,
  facebookSigninController.handler,
);
router.post('/signin-google', googleSigninController.validator, googleSigninController.handler);

export default router;
