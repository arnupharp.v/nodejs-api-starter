import random from 'randomstring';
import {
  createLocalUser,
  getUserByEmail,
  getLocalUserByResetPasswordToken,
  updateUserById,
  createThirdPartyUser,
  getUserById,
  getAllUsers,
  getUsersByEmails,
  getUserByProviderId,
  getUserByRefreshTokenId,
} from './users-data';
import {
  getActiveConsentFormService,
  getConsentFormByChecksumService,
} from '../consents/consents-form-service';
import {
  applyUserConsentsService,
  extendUserConsentsExpiryDateService,
  getUserConsentedKeysService,
} from '../consents/consents-user-service';

import { duplicateError, unauthorized, validationError, notFound } from '../../helpers/error';
import { sendEmail } from '../../helpers/email';
import {
  getSignupVerifyEmailTemplate,
  getResetPasswordEmailTemplate,
} from '../../templates/templates-email';

import {
  createPasswordHash,
  createRefreshToken,
  createAccessToken,
  isPasswordMatchHash,
  removeSensitiveFields,
  decodeRefreshToken,
  isRefreshTokenValid,
} from './users-domain';
import { getFbUserByAccessToken } from '../../connections/fb';
import {
  AUTH_STATUS_LOCKED,
  AUTH_STATUS_VERIFIED,
  AUTH_STATUS_WAIT_VERIFY,
  PAGINATION_DEFAULT_LIMIT,
  STATUS_ACTIVE,
  STATUS_DELETED,
} from '../../helpers/constant';
import { getCurrentTime } from '../../helpers/datetime';
import { getGoogleUserByIdToken } from '../../connections/google';
import { getRolesByUserIdService } from '../roles/roles-service';
import { clearUserRolesById, createUserRoles } from './users-roles-data';

export async function ensureUserEmailNotExistService(email) {
  const existing = await getUserByEmail(email);
  if (existing) {
    throw duplicateError();
  }
  return true;
}

export async function ensureUserExistsService(id) {
  const user = await getUserById(id);
  if (!user) {
    throw notFound();
  }
  return user;
}

async function toSignInCredential(user) {
  const { id, email, refreshTokenId, refreshTokenSecret, passwordHash, consentFormChecksum } = user;

  // Fetching user roles
  const roleIds = [];
  const roles = await getRolesByUserIdService(id);
  roles.data.forEach((r) => {
    roleIds.push(r.id);
  });

  const refreshToken = createRefreshToken(refreshTokenId, refreshTokenSecret, passwordHash);
  const accessToken = await createAccessToken(id, email, roleIds);

  const updated = await updateUserById(id, { lastLoginAt: getCurrentTime() });

  const consentedKeys = await getUserConsentedKeysService(id, consentFormChecksum);

  // Automatically extend expiry date of user consents (can be run in background)
  extendUserConsentsExpiryDateService(id, consentFormChecksum);

  return { user: { ...removeSensitiveFields(updated), consentedKeys }, accessToken, refreshToken };
}

export async function applyUserRolesService(userId, roleIds) {
  await clearUserRolesById(userId);
  if (!roleIds || !roleIds.length) return;

  const userRoles = [];
  roleIds.forEach((roleId) => {
    userRoles.push({
      userId,
      roleId,
    });
  });

  await createUserRoles(userRoles);
}

export async function createLocalUserService(
  {
    email,
    password,
    firstName,
    lastName,
    telephone,
    status,
    consentedKeys,
    consentFormChecksum,
    createdBy,
    roleIds,
  },
  skipValidation = false,
) {
  await ensureUserEmailNotExistService(email);

  const consentForm = await getConsentFormByChecksumService(consentFormChecksum);
  if (!consentForm || consentForm.status !== STATUS_ACTIVE) {
    throw validationError('Invalid consent form.');
  }

  const requireVerify =
    !skipValidation && (process.env.APP_SIGNUP_REQUIRE_VERIFY || 'false').toLowerCase() === 'true';

  const created = await createLocalUser({
    email,
    password,
    firstName,
    lastName,
    telephone,
    consentFormChecksum,
    isAdmin: false,
    status: status || (requireVerify ? AUTH_STATUS_WAIT_VERIFY : AUTH_STATUS_VERIFIED),
    createdBy,
  });

  // Store consent data
  await applyUserConsentsService({ userId: created.id, consentForm, consentedKeys });

  if (roleIds && roleIds.length) {
    await applyUserRolesService(created.id, roleIds);
  }

  if (requireVerify) {
    // Send verification email
    const url = `${process.env.FRONTEND_URL}/accounts/verify?verifyToken=${created.resetPasswordToken}`;
    const emailTemplate = getSignupVerifyEmailTemplate({ firstName, lastName, url });

    sendEmail({ ...emailTemplate, receivers: [email] }); // Shoud we await for it?

    // Return user data only
    return { user: removeSensitiveFields(created) };
  }

  // Return user data and access token.
  return toSignInCredential(created);
}

// Update own profile by himself
export async function updateProfileService({
  id,
  email,
  gender,
  firstName,
  lastName,
  telephone,
  birthDate,
}) {
  await ensureUserExistsService(id);
  const existingByEmail = await getUserByEmail(email);

  if (existingByEmail && existingByEmail.id !== id) {
    // Email of all local users must be unique.
    throw duplicateError();
  }

  const updated = await updateUserById(id, {
    email,
    gender,
    firstName,
    lastName,
    telephone,
    birthDate,
    updatedAt: getCurrentTime(),
    updatedBy: id,
  });

  return { user: removeSensitiveFields(updated) };
}

export async function updateUserByIdService(
  id,
  { email, gender, firstName, lastName, telephone, birthDate, status, roleIds, updatedBy },
) {
  await ensureUserExistsService(id);

  if (email) {
    const existingByEmail = await getUserByEmail(email);

    if (existingByEmail && existingByEmail.id !== id) {
      // Email of all local users must be unique.
      throw duplicateError();
    }
  }

  const updated = await updateUserById(id, {
    email,
    gender,
    firstName,
    lastName,
    telephone,
    birthDate,
    status,
    updatedAt: getCurrentTime(),
    updatedBy: updatedBy || id,
  });

  if (roleIds) {
    await applyUserRolesService(updated.id, roleIds);
  }

  return { user: removeSensitiveFields(updated) };
}

export async function deleteUserByIdService(id, deletedBy) {
  await updateUserById(id, {
    status: STATUS_DELETED,
    updatedAt: getCurrentTime(),
    updatedBy: deletedBy,
  });
}

export async function signinService({ email, password }) {
  const user = await getUserByEmail(email);
  if (!user || user.status !== AUTH_STATUS_VERIFIED) {
    throw unauthorized();
  }
  const passwordCorrect = await isPasswordMatchHash(password, user.passwordHash);
  if (!passwordCorrect) {
    throw unauthorized();
  }

  return toSignInCredential(user);
}

export async function verifyLocalUserService({ verifyToken }) {
  let user = await getLocalUserByResetPasswordToken(verifyToken);
  if (typeof user === 'undefined' || user.status !== AUTH_STATUS_WAIT_VERIFY) {
    throw unauthorized();
  }

  // Update user status to AUTH_STATUS_VERIFIED
  user = await updateUserById(user.id, {
    status: AUTH_STATUS_VERIFIED,
    verifiedAt: getCurrentTime(),
    lastLoginAt: getCurrentTime(),
    resetPasswordToken: null,
  });

  const consentedKeys = await getUserConsentedKeysService(user.id, user.consentFormChecksum);

  return { user: { ...removeSensitiveFields(user), consentedKeys } };
}

export async function renewAccessTokenService(refreshToken) {
  try {
    const decoded = decodeRefreshToken(refreshToken);
    const user = await getUserByRefreshTokenId(decoded.refreshTokenId);
    if (typeof user === 'undefined') {
      throw unauthorized();
    }

    if (
      !isRefreshTokenValid(
        decoded.signature,
        user.refreshTokenId,
        user.refreshTokenSecret,
        user.passwordHash,
      )
    ) {
      throw unauthorized();
    }

    const accessToken = await createAccessToken(user.id, user.email, user.isAdmin ? ['admin'] : []);
    return accessToken;
  } catch (error) {
    throw unauthorized();
  }
}

export async function facebookSigninService({ fbAccessToken }) {
  const fbuser = await getFbUserByAccessToken(fbAccessToken);
  const consentForm = await getActiveConsentFormService();
  const { consentFormChecksum } = consentForm;

  let user = await getUserByProviderId('facebook', fbuser.id);
  if (!user) {
    const { id, email, first_name: firstName, last_name: lastName } = fbuser;

    // Check if email already exists
    user = await getUserByEmail(email);
    if (user) {
      // User cannot register with the same email
      throw validationError(`Duplicate email exists on database`);
    } else {
      user = await createThirdPartyUser({
        id,
        provider: 'facebook',
        email: email || id,
        firstName,
        lastName,
        consentFormChecksum,
      });

      // Assume that user accept all consent keys (can be changed later by user).
      const consentedKeys = [];
      consentForm.context.consentItems.forEach((o) => {
        consentedKeys.push(o.key);
      });

      await applyUserConsentsService({ userId: user.id, consentForm, consentedKeys });
    }
  }

  if (user.status !== AUTH_STATUS_VERIFIED) {
    throw unauthorized();
  }

  return toSignInCredential(user);
}

export async function googleSigninService({ idToken }) {
  const googleUser = await getGoogleUserByIdToken(idToken);
  const consentForm = await getActiveConsentFormService();
  const { consentFormChecksum } = consentForm;

  let user = await getUserByProviderId('google', googleUser.sub);
  if (!user) {
    const { sub: id, email, given_name: firstName, family_name: lastName } = googleUser;

    // Check if email already exists
    user = await getUserByEmail(email);
    if (user) {
      // User cannot register with the same email
      throw validationError(`Duplicate email exists on database`);
    } else {
      user = await createThirdPartyUser({
        id,
        provider: 'google',
        email: email || id,
        firstName,
        lastName,
        consentFormChecksum,
      });

      // Assume that user accept all consent keys (can be changed later by user).
      const consentedKeys = [];
      consentForm.context.consentItems.forEach((o) => {
        consentedKeys.push(o.key);
      });

      await applyUserConsentsService({ userId: user.id, consentForm, consentedKeys });
    }
  }

  if (user.status !== AUTH_STATUS_VERIFIED) {
    throw unauthorized();
  }

  return toSignInCredential(user);
}

export async function createResetPasswordLinkService(email) {
  const user = await getUserByEmail(email);

  if (!user) throw notFound();
  if (user.status === AUTH_STATUS_LOCKED) throw unauthorized();

  const resetPasswordToken = random.generate(50);
  await updateUserById(user.id, {
    resetPasswordToken,
    updatedBy: user.id,
    updatedAt: getCurrentTime(),
  });

  // Send reset password email
  const { firstName, lastName } = user;
  const url = `${process.env.FRONTEND_URL}/accounts/reset-password?resetToken=${resetPasswordToken}`;
  const emailTemplate = getResetPasswordEmailTemplate({ firstName, lastName, url });

  sendEmail({ ...emailTemplate, receivers: [email] }); // Shoud we await for it?

  return { message: `Reset password link has been sent to email ${email}`, email };
}

export async function resetPasswordService(password, resetToken) {
  const user = await getLocalUserByResetPasswordToken(resetToken);
  if (!user) throw notFound();

  if (user.status === AUTH_STATUS_LOCKED) throw unauthorized();

  await updateUserById(user.id, {
    passwordHash: createPasswordHash(password),
    resetPasswordToken: null,
    status: AUTH_STATUS_VERIFIED,
    verifiedAt: user.verifiedAt || getCurrentTime(),
    updatedBy: user.id,
    updatedAt: getCurrentTime(),
  });

  return { message: 'You password has been reset' };
}

export async function updatePasswordService(id, oldPassword, newPassword) {
  const user = await ensureUserExistsService(id);

  if (user.status !== AUTH_STATUS_VERIFIED) throw unauthorized();

  const passwordCorrect = await isPasswordMatchHash(oldPassword, user.passwordHash);
  if (!passwordCorrect) {
    throw unauthorized();
  }

  await updateUserById(user.id, {
    passwordHash: createPasswordHash(newPassword),
    resetPasswordToken: null,
    updatedBy: user.id,
    updatedAt: getCurrentTime(),
  });

  return { message: 'You password has been updated' };
}

export async function getUserByIdService(id, loadConsents = true, loadRoles = false) {
  const user = await getUserById(id);
  if (user && loadConsents) {
    user.consentedKeys = await getUserConsentedKeysService(user.id, user.consentFormChecksum);
  }

  if (user && loadRoles) {
    user.roles = (await getRolesByUserIdService(id)).data;
  }

  return user;
}

export async function getUsersByEmailsService(emails) {
  const users = await getUsersByEmails(emails);
  return users;
}

export async function getUserByRefreshTokenService(refreshToken) {
  const decoded = decodeRefreshToken(refreshToken);
  const user = await getUserByRefreshTokenId(decoded.refreshTokenId);
  if (typeof user === 'undefined') {
    throw unauthorized();
  }

  if (
    !isRefreshTokenValid(
      decoded.signature,
      user.refreshTokenId,
      user.refreshTokenSecret,
      user.passwordHash,
    )
  ) {
    throw unauthorized();
  }
  return user;
}

export async function getAllUsersService({
  offset,
  limit,
  filter,
  sortBy = 'firstName',
  sortDirection = 'asc',
}) {
  const opt = {
    offset,
    limit,
    filter,
    sorts: [{ column: sortBy, direction: sortDirection }],
  };

  if (!opt.offset) opt.offset = 0;
  if (!opt.limit) opt.limit = PAGINATION_DEFAULT_LIMIT;

  const result = await getAllUsers(opt);
  for (let i = 0; i < result.data.length; i += 1) {
    result.data[i] = removeSensitiveFields(result.data[i]);
  }

  return result;
}
