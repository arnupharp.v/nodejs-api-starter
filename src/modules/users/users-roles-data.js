import knex from '../../connections/knex';
import { databaseError } from '../../helpers/error';

const TABLE_NAME = 'user_roles';

export async function createUserRoles(userRoles) {
  try {
    return knex(TABLE_NAME).insert(userRoles);
  } catch (error) {
    throw databaseError(error);
  }
}

export async function clearUserRolesById(userId) {
  try {
    await knex(TABLE_NAME).where({ userId }).delete();
  } catch (error) {
    throw databaseError(error);
  }
}
