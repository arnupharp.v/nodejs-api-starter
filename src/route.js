import * as express from 'express';
import categoriesRoute from './modules/categories/categories-route';
import consentsRoute from './modules/consents/consents-route';
// import eventsRoute from './modules/events/events-route';
import postsRoute from './modules/posts/posts-route';
import testsRoute from './modules/tests/tests-route';
import usersRoute from './modules/users/users-route';

import postsBackofficeRoute from './modules/posts/posts-backoffice-route';
import usersBackofficeRoute from './modules/users/users-backoffice-route';
import rolesBackofficeRoute from './modules/roles/roles-backoffice-route';

const router = express.Router();

router.use('/categories', categoriesRoute);
router.use('/consents', consentsRoute);
// router.use('/events', eventsRoute);
router.use('/posts', postsRoute);
router.use('/tests', testsRoute);
router.use('/users', usersRoute);

export default router;
