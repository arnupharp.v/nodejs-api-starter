import { Model } from 'objection';

const knex = require('knex');

const knexfile = require('../../knexfile');

const env = process.env.NODE_ENV || 'development';
const configOptions = knexfile[env];

const knexInstance = knex(configOptions);
Model.knex(knexInstance);

module.exports = knexInstance;
