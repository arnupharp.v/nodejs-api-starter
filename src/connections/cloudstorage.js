import { Storage } from '@google-cloud/storage';

const storage = new Storage({ keyFilename: process.env.GCP_SERVICE_ACCOUNT });

export async function uploadToCloudStorage(srcFileName, bucket, option) {
  const defaultOption = {
    gzip: true,
    public: true,
    metadata: {
      // Enable long-lived HTTP caching headers
      // Use only if the contents of the file will never change
      // (If the contents will change, use cacheControl: 'no-cache')
      cacheControl: 'public, max-age=31536000',
    },
  };
  const result = await storage.bucket(bucket).upload(srcFileName, option || defaultOption);
  return result;
}
