import fetch from 'isomorphic-unfetch';
import { externalError, unauthorized } from '../helpers/error';

export function getGoogleUserByIdToken(idToken) {
  return fetch(`https://oauth2.googleapis.com/tokeninfo?id_token=${idToken}`)
    .then((r) => r.json())
    .then((json) => {
      if (json.error) {
        console.log(json.error);
        throw unauthorized();
      }
      return json;
    })
    .catch((error) => {
      externalError(error);
    });
}
