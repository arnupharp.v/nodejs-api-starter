import fetch from 'isomorphic-unfetch';
import { externalError, unauthorized } from '../helpers/error';

export function getFbUserByAccessToken(accessToken) {
  return fetch(
    `https://graph.facebook.com/v9.0/me?fields=id,email,first_name,last_name&access_token=${accessToken}`,
  )
    .then((r) => r.json())
    .then((json) => {
      if (json.error) {
        console.log(json.error);
        throw unauthorized();
      }
      return json;
    })
    .catch((error) => {
      externalError(error);
    });
}
