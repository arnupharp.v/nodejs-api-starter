import { validationResult } from 'express-validator';
import { validationError, validationErrorFormatter } from './error';

// eslint-disable-next-line import/prefer-default-export
export function validate(validators) {
  return [].concat(
    /* istanbul ignore next */
    validators || [],
    (req, _, next) => {
      const error = validationResult(req);
      if (error.isEmpty()) next();
      else {
        const errorMessage = error
          .formatWith(validationErrorFormatter)
          .array({ onlyFirstError: true })
          .join(', ');
        validationError(errorMessage, next);
      }
    },
  );
}
