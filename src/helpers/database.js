import { transaction } from 'objection';
import { databaseError } from './error';

export default async function startTransaction(arg1, arg2, arg3) {
  try {
    if (typeof arg3 === 'undefined') {
      if (typeof arg2 === 'function') {
        return await transaction(arg1, async (arg1) => {
          const fnTrx = await arg2(arg1);
          return fnTrx;
        });
      }
    } else {
      return await transaction(arg1, arg2, async (arg1, arg2) => {
        const fnTrx = await arg3(arg1, arg2);
        return fnTrx;
      });
    }

    return false;
  } catch (err) {
    if (typeof arg2 === 'function') {
      throw databaseError(err, arg1);
    } else {
      throw databaseError(err, { arg1, arg2 });
    }
  }
}

export function serializeJsonbValue(jsonbValue) {
  if (typeof jsonbValue === 'undefined') return undefined;
  if (!jsonbValue) return jsonbValue;

  const { DB_PROVIDER } = process.env;
  if (DB_PROVIDER === 'postgresql') {
    return typeof jsonbValue === 'string' ? jsonbValue : JSON.stringify(jsonbValue);
    // return typeof jsonbValue === 'string' ? JSON.parse(jsonbValue) : jsonbValue;
  }

  return typeof jsonbValue === 'string' ? jsonbValue : JSON.stringify(jsonbValue);
}

export function deserializeJsonbValue(jsonbValue) {
  if (!jsonbValue) return jsonbValue;

  const { DB_PROVIDER } = process.env;
  if (DB_PROVIDER === 'postgresql') {
    return typeof jsonbValue === 'string' ? JSON.parse(jsonbValue) : jsonbValue;
  }

  return typeof jsonbValue === 'string' ? JSON.parse(jsonbValue) : jsonbValue;
}
