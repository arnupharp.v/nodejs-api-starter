import { BACKOFFICE_BASE_PATH } from './constant';

export function isForBackOffice(req) {
  return req.baseUrl.search(BACKOFFICE_BASE_PATH) >= 0;
}
