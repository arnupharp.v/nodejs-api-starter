const crypto = require('crypto');

export function md5(text) {
  return crypto.createHash('md5').update(text).digest('hex');
}

export function randomString(len) {
  return crypto.randomBytes(len).toString('hex');
}
