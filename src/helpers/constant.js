export const AUTH_STATUS_UNVERIFIED = 'unverified';
export const AUTH_STATUS_WAIT_VERIFY = 'wait_verify';
export const AUTH_STATUS_VERIFIED = 'verified';
export const AUTH_STATUS_LOCKED = 'locked';

export const STATUS_PENDING = 'pending';
export const STATUS_ACTIVE = 'active';
export const STATUS_INACTIVE = 'inactive';
export const STATUS_DELETED = 'deleted';
export const STATUS_COMPLETED = 'completed';
export const STATUS_PUBLISHED = 'published';

export const ROLE_STATUS_ACTIVE = 'active';
export const ROLE_STATUS_DELETED = 'deleted';
export const ROLE_TYPE_CUSTOM = 'custom';
export const ROLE_TYPE_SYSTEM = 'system';

export const POST_STATUS_DRAFT = 'draft';
export const POST_STATUS_PUBLISHED = 'published';
export const POST_STATUS_UNPUBLISHED = 'unpublished';
export const POST_STATUS_DELETED = 'deleted';

export const PAGINATION_DEFAULT_LIMIT = 50;

export const BACKOFFICE_BASE_PATH = '/backoffice/';
