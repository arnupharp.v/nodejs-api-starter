import base64Img from 'base64-img';
import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import fs from 'fs';

export const uploadImageBase64 = (pathUpload, imageBase64) => {
  const fileName = uuidv4();
  const filePath = base64Img.imgSync(imageBase64, pathUpload, fileName);
  return path.basename(filePath);
};

export const downloadImageBase64 = (pathUpload, fileName) => {
  try {
    if (fs.existsSync(`${pathUpload}/${fileName}`)) {
      return base64Img.base64Sync(`${pathUpload}/${fileName}`);
    }

    return null;
  } catch (err) {
    return err;
  }
};
