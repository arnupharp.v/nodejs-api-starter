import { externalError } from './error';

const nodemailer = require('nodemailer');
const emailValidator = require('email-validator');

export function isEmailValid(email) {
  return emailValidator.validate(email);
}

export async function sendEmail({ subject, body, receivers }) {
  try {
    const transporter = nodemailer.createTransport({
      host: process.env.SMTP_SERVER,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
        user: process.env.SMTP_SENDER_USERNAME,
        pass: process.env.SMTP_SENDER_PASSWORD,
      },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
      from: process.env.SMTP_SENDER_FROM, // sender address
      to: receivers.join(), // list of receivers
      subject,
      // text: "Hello world?", // plain text body
      html: body,
    });

    console.log('Message sent: %s', info.messageId);
  } catch (error) {
    throw externalError(error);
  }
}
