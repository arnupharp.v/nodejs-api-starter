import fs from 'fs';
import { uploadToCloudStorage } from '../connections/cloudstorage';

export async function uploadFile(file, { fileKey, fileGroup, deleteExisting = true }) {
  const filename = `${new Date().getTime()}_${file.name}`;
  const path = `./public/upload/${filename}`;

  console.log({ file, fileKey, fileGroup, deleteExisting });

  return new Promise((resolve, reject) => {
    file.mv(path).then((err) => {
      if (err) {
        console.log('Upload file error: ', err);
        reject(err);
      } else {
        if (deleteExisting) {
          // Do something to delete existing file.
        }

        // File upload to server success
        if (process.env.USE_CLOUDSTORAGE !== 'true') {
          const url = `${process.env.BACKEND_URL}/upload/${filename}`;
          resolve(url);
        } else {
          uploadToCloudStorage(path, process.env.BUCKET_NAME)
            .then(() => {
              const url = `https://storage.googleapis.com/${process.env.BUCKET_NAME}/${filename}`;
              fs.unlink(path, (error) => {
                if (error) {
                  reject(error);
                }
                resolve(url);
              });
            })
            .catch((error) => {
              reject(error);
            });
        }
      }
    });
  });
}
