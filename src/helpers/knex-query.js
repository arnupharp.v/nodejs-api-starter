import knex from '../connections/knex';

export function getLikeOperator() {
  const { DB_PROVIDER } = process.env;
  if (DB_PROVIDER === 'postgresql') {
    return 'ilike';
  }

  return 'like';
}

export function getFieldName(field) {
  const { DB_PROVIDER } = process.env;
  if (DB_PROVIDER === 'postgresql') {
    const parts = field.split('.');
    if (parts.length > 1) return `"${parts[0]}"."${parts[1]}"`;
    return `"${field}"`;
  }

  return `${field}`;
}

export async function knexQueryRange(tableName, conditionBuilders, { offset, limit, sorts = [] }) {
  const queryBuilder = knex(tableName).select('*');
  const queryBuilderCount = knex(tableName);
  conditionBuilders.forEach((builder) => {
    queryBuilder.where(builder);
    queryBuilderCount.where(builder);
  });

  (sorts || []).forEach(({ column, direction }) => queryBuilder.orderBy(column, direction));

  console.log(queryBuilder.toSQL().toNative());

  const counter = await queryBuilderCount.count('* as total');
  const data = await queryBuilder.limit(limit).offset(offset);

  return {
    data,
    meta: {
      total: counter[0].total,
      offset,
      limit,
    },
  };
}

export async function knexQueryRangeWithAlias(
  { name, query, params },
  conditionBuilders,
  { offset, limit, sorts = [] },
) {
  const queryBuilder = knex.with(name, knex.raw(query, params)).select('*').from(name);
  const queryBuilderCount = knex.with(name, knex.raw(query, params)).from(name);
  conditionBuilders.forEach((builder) => {
    queryBuilder.where(builder);
    queryBuilderCount.where(builder);
  });

  (sorts || []).forEach(({ column, direction }) => queryBuilder.orderBy(column, direction));

  console.log(queryBuilder.toSQL().toNative());

  const counter = await queryBuilderCount.count('* as total');
  const data = await queryBuilder.limit(limit).offset(offset);

  return {
    data,
    meta: {
      total: counter[0].total,
      offset,
      limit,
    },
  };
}

export async function commitOnSuccess(transactionScope) {
  await knex.transaction(transactionScope);
}
