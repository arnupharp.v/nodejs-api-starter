import { getRolesByIdsService } from '../modules/roles/roles-service';
import { mergeDedupe } from './array';
import { forbidden } from './error';

/**
 * Extract permissions granted to a user on a specified project
 * @param {Credential} roleIds - roleIds extracted from access token
 * @return {Promise<string[]>} - Array of permission list granted to a user on a specified project
 */
export async function _extractPermission(roleIds) {
  if (typeof roleIds === 'undefined') {
    return [];
  }
  const roles = await getRolesByIdsService(roleIds);
  const permissions = mergeDedupe(roles.map((role) => role.scopes));

  return permissions;
}

/**
 * Create a regular expression that will capture permission strings that match required permissions
 * eg.
 * case 1: 'a.b.c' => '(a|\*)\.(b|\*)\.(c|\*)'
 * case 2: 'a.*.c' => '(a|\*)\.([\w\d]+|\*)\.(c|\*)'
 * case 3: ['a.b.c', 'a.d.c] => '(a|\*)\.(b|\*)\.(c|\*)|(a|\*)\.(d|\*)\.(c|\*)'
 * @param {string | string[]} permissions - List of required permissions
 * @return {string} - Regular expressesion string that can be passed as a parameter to create RegExp object.
 *    The created RegExp will return true if tested text match one of required permissions
 */
export function _requiredPermissionRegEx(permissions) {
  // If permissions is a string return array with that string
  const _permissions = typeof permissions === 'string' ? [permissions] : permissions;

  // case 1: 'a.b.c' => '(a|\*)\.(b|\*)\.(c|\*)'
  // case 2: 'a.*.c' => '(a|\*)\.([\w\d]+|\*)\.(c|\*)'
  // case 3: ['a.b.c', 'a.d.c] => '(a|\*)\.(b|\*)\.(c|\*)|(a|\*)\.(d|\*)\.(c|\*)'
  const regex = _permissions
    .map((permission) => {
      const [group, scope, action] = permission
        .split('.')
        .map((selector) => (selector === '*' ? '[\\w\\d]+' : selector));
      return `(${group}|\\*)\\.(${scope}|\\*)\\.(${action}|\\*)`;
    })
    .join('|');

  return regex;
}

/**
 * A middleware for checking wheter a user has permissions neccesary to use an endpoint or not
 * @param requiredPermissions
 */
export function checkPermission(requiredPermissions) {
  return async (req, res, next) => {
    const grantedPermissions = await _extractPermission(req?.credential?.roleIds);
    const regexString = _requiredPermissionRegEx(requiredPermissions);
    // console.log(`grantedPermissions: ${JSON.stringify(grantedPermissions)}`);
    // console.log(`regexString: ${regexString}`);
    const regex = new RegExp(`^(${regexString})$`, 'i');
    if (grantedPermissions.some((grantedPermission) => regex.test(grantedPermission))) next();
    else forbidden(next);
  };
}

export async function checkPermissionByCredential(credential, requiredPermissions) {
  const grantedPermissions = await _extractPermission(credential?.roleIds);
  const regexString = _requiredPermissionRegEx(requiredPermissions);
  const regex = new RegExp(`^(${regexString})$`, 'i');
  if (grantedPermissions.some((grantedPermission) => regex.test(grantedPermission))) {
    return true;
  }

  return false;
}

export function requiredAuth() {
  return async (req, res, next) => {
    if (!req.credential || !req.credential.id) {
      forbidden(next);
    } else {
      next();
    }
  };
}

export function checkRoles(requiredRoleIds) {
  return async (req, res, next) => {
    const grantedRoleIds = req?.credential?.roleIds || [];
    if (grantedRoleIds.some((roleId) => requiredRoleIds.include(roleId))) next();
    else forbidden(next);
  };
}

export const ROLE = {
  ADMIN: 1,
};
