/**
 * Merge member of inputs and dedupe duplicate item eg. for items = [[1,2,3], [4,5], [1,6,7]] will return [1,2,3,4,5,6,7]
 * @param inputs - Array of array eg. [[1,2,3], [4,5], [1,6,7]]
 * @returns Dedupe merged array
 */
export function mergeDedupe(inputs) {
  const merged = [].concat(...inputs);
  const dedupe = Array.from(new Set(merged));
  return dedupe;
}
