import moment from 'moment';

require('moment/locale/th');

export function getCurrentTime() {
  return moment().format('YYYY-MM-DD HH:mm:ss');
  // return moment().toISOString();
}

export function addDays(days, date = new Date()) {
  date.setDate(date.getDate() + parseInt(days, 10));
  return date;
}

export function addMinutes(minutes, date = new Date()) {
  const newDate = moment(date).add(minutes, 'm').toDate();
  return newDate;
}

export function formatForDB(date) {
  return moment(date).format('YYYY-MM-DD HH:mm:ss');
}

export function formatToThaiDate(date) {
  moment.locale('th');
  let d = moment(date);
  if (d.year() < 2500) {
    d = d.add(543, 'years');
  }

  return d.format('D MMMM YYYY');
}
