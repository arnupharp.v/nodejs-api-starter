/** Error messages to be thrown. */
export const errorMessage = {
  BAD_REQUEST: 'BAD_REQUEST',
  UNSUPPORT_CONTENT_TYPE: 'Unsupported Media Type',
  NOT_FOUND: 'Not Found',
  FORBIDDEN: 'Forbidden',
  VALIDATION_FAIL: 'Validation failed',
  UNPROCESSABLE_ENTITY: 'Unprocessable Entity',
  INVALID_JWT: 'Invalid JWT',
  INVALID_ID_TOKEN: 'Invalid ID Token',
  DUPLICATE_KEY: 'Duplicate item exists on database',
  DATABASE_ERROR: 'Database Error',
  INTERNAL_ERROR: 'Internal Server Error',
  EXTERNAL_ERROR: 'ExternalError',
  INVALID_TOKEN: 'Invalid token',
  UNAUTHORIZED: 'Unauthorized user',
  EXPIRED_TOKEN: 'Expired token',
};

class CustomError extends Error {
  constructor({ status, simplifiedMessage, message, stack, context }) {
    super(message);
    this.status = status;
    this.simplifiedMessage = simplifiedMessage;
    this.message = message || simplifiedMessage;
    this.stack = stack;
    this.context = context;
  }
}

export function badRequest(simplifiedMessage) {
  const error = new CustomError({
    status: 400,
    simplifiedMessage: simplifiedMessage || errorMessage.BAD_REQUEST,
  });

  throw error;
}

export function validationError(simplifiedMessage) {
  const error = new CustomError({
    status: 400,
    simplifiedMessage: simplifiedMessage || errorMessage.VALIDATION_FAIL,
  });

  throw error;
}

export function invalidToken(simplifiedMessage) {
  const error = new CustomError({
    status: 400,
    simplifiedMessage: simplifiedMessage || errorMessage.INVALID_TOKEN,
  });

  throw error;
}

export function idTokenError(simplifiedMessage) {
  const error = new CustomError({
    status: 400,
    simplifiedMessage: simplifiedMessage || errorMessage.INVALID_ID_TOKEN,
  });

  throw error;
}

export function jwtError(simplifiedMessage) {
  const error = new CustomError({
    status: 401,
    simplifiedMessage: simplifiedMessage || errorMessage.INVALID_JWT,
  });

  throw error;
}

export function unauthorized(simplifiedMessage) {
  const error = new CustomError({
    status: 401,
    simplifiedMessage: simplifiedMessage || errorMessage.UNAUTHORIZED,
  });

  throw error;
}

export function expiredToken(simplifiedMessage) {
  const error = new CustomError({
    status: 401,
    simplifiedMessage: simplifiedMessage || errorMessage.EXPIRED_TOKEN,
  });

  throw error;
}

export function forbidden(simplifiedMessage) {
  const error = new CustomError({
    status: 403,
    simplifiedMessage: simplifiedMessage || errorMessage.FORBIDDEN,
  });

  throw error;
}

export function notFound(simplifiedMessage) {
  const error = new CustomError({
    status: 404,
    simplifiedMessage: simplifiedMessage || errorMessage.NOT_FOUND,
  });

  throw error;
}

export function unsupportContentType(simplifiedMessage) {
  const error = new CustomError({
    status: 415,
    simplifiedMessage: simplifiedMessage || errorMessage.UNSUPPORT_CONTENT_TYPE,
  });

  throw error;
}

export function duplicateError(simplifiedMessage) {
  const error = new CustomError({
    status: 422,
    simplifiedMessage: simplifiedMessage || errorMessage.DUPLICATE_KEY,
  });

  throw error;
}

export function unprocessable(simplifiedMessage) {
  const error = new CustomError({
    status: 422,
    simplifiedMessage: simplifiedMessage || errorMessage.UNPROCESSABLE_ENTITY,
  });

  throw error;
}

export function internalError(error, context, message) {
  const _error = new CustomError({
    status: 500,
    simplifiedMessage: errorMessage.INTERNAL_ERROR,
    message: message || error.message,
    stack: error.stack,
    context,
  });

  throw _error;
}

export function externalError(error, context, message) {
  const _error = new CustomError({
    status: 500,
    simplifiedMessage: errorMessage.EXTERNAL_ERROR,
    message: message || error.message,
    stack: error.stack,
    context,
  });

  throw _error;
}

export function databaseError(error, context, message) {
  const _error = new CustomError({
    status: 500,
    simplifiedMessage: errorMessage.DATABASE_ERROR,
    message: message || error.message,
    stack: error.stack,
    context,
  });

  throw _error;
}

export function validationErrorFormatter({ msg, param }) {
  return `'${param}' ${msg}`;
}
