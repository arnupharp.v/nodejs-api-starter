import logger from './logger';

const fs = require('fs');
const mime = require('mime-types');

export function send(payload, req, res) {
  if (payload.code >= 500) {
    logger.error('[RESPONSE(SERVER ERROR)]', payload);
  } else if (payload.code >= 400) {
    logger.info('[RESPONSE(CLIENT ERROR)]', payload);
  } else {
    logger.debug('[RESPONSE(SUCCESS)]', payload);
  }
  res.status(payload.code);
  res.send(payload);
}

export function sendFile(filePath, res, downloadAsName) {
  const stat = fs.statSync(filePath);
  const mimeType = mime.lookup(filePath);

  const header = {
    'Content-Type': mimeType,
    'Content-Length': stat.size,
    'Cache-Control': 'no-store, no-cache, must-revalidate, proxy-revalidate',
    Pragma: 'no-cache',
    Expires: '0',
  };

  if (downloadAsName) header['Content-Disposition'] = `attachment; filename=${downloadAsName}`;

  res.writeHead(200, header);

  const readStream = fs.createReadStream(filePath);
  // We replaced all the event handlers with a simple call to readStream.pipe()
  readStream.pipe(res);
}

export function setCookie(name, val, option, res) {
  const _option = { ...option };
  // Allow cookie to be sent over http during development
  if (process.env.NODE_ENV === 'development') {
    _option.secure = false;
  }
  res.cookie(name, val, _option);
}

export function toPaginationObject(paginatedData, paginationConfig) {
  let {
    limit = Number(process.env.PAGINATION_DEFAULTLIMIT),
    offset = Number(process.env.PAGINATION_DEFAULTOFFSET),
    total,
  } = paginationConfig;
  limit = Number(limit);
  offset = Number(offset);
  total = Number(total);
  return {
    meta: { limit, offset, total },
    data: paginatedData,
  };
}
