export async function queryRange(model, conditionBuilder, option) {
  const counter = await model.query().where(conditionBuilder).count('* as total');

  const data = await model
    .query()
    .where(conditionBuilder)
    .limit(option.limit)
    .offset(option.offset)
    .orderBy(option.sortBy, option.sortDirection);

  return {
    data,
    meta: {
      total: counter[0].total,
      offset: option.offset,
      limit: option.limit,
    },
  };
}

export async function queryRangeWithModifier(
  model,
  modifier,
  modifierParams,
  fields,
  conditionBuilder,
  option,
) {
  const counter = await model
    .query()
    .modify(modifier, modifierParams)
    .where(conditionBuilder)
    .count('* as total');

  const data = await model
    .query()
    .modify(modifier, modifierParams)
    .where(conditionBuilder)
    .select(fields)
    .limit(option.limit)
    .offset(option.offset)
    .orderBy(option.sortBy, option.sortDirection);

  return {
    data,
    meta: {
      total: counter[0].total,
      offset: option.offset,
      limit: option.limit,
    },
  };
}
