/**
 * Logging module
 * (Do not ever use `console.log`, use this module to create an instance and log with it.)
 * @module helpers/log
 */

import * as winston from 'winston';
import DailyFile from 'winston-daily-rotate-file';
import httpContenxt from 'express-http-context';
import fs from 'fs';
import path from 'path';

const logDir = path.resolve('logs');

/* istanbul ignore if */
if (!fs.existsSync(logDir)) fs.mkdirSync(logDir);

/* istanbul ignore next */
const logLevel = process.env.LOG_LEVEL || 'silly';
const { timestamp, combine, json, colorize, printf } = winston.format;

/**
 * Automatically add reqId from http context to info
 */
const reqId = winston.format((info) => ({
  ...info,
  reqId: httpContenxt.get('reqId'),
}));

const simpleprint = printf(({ reqId, timestamp, level, message, ...meta }) => {
  const log =
    // eslint-disable-next-line
    `[${timestamp}] [${level}] [${reqId}] ` +
    (message || '') +
    (meta && Object.keys(meta).length ? `\n\t${JSON.stringify(meta)}` : '');
  return log;
});

let transports = [];
let format;

const { NODE_ENV } = process.env;
if (NODE_ENV === 'production') {
  transports = [
    new winston.transports.Console({ level: logLevel }),
    new DailyFile({
      level: logLevel,
      filename: `${process.env.LOG_FILENAME || 'access'}-%DATE%.log`,
      dirname: 'logs',
      datePattern: 'YYYYMMDD',
      zippedArchive: true,
    }),
  ];
  format = combine(reqId(), timestamp(), json());
} else if (NODE_ENV === 'test') {
  transports = [new winston.transports.Console({ silent: true })];
} else {
  transports = [new winston.transports.Console({ level: logLevel })];
  format = combine(reqId(), timestamp(), colorize(), simpleprint);
}

const logger = winston.createLogger({
  format,
  transports,
  exitOnError: false,
});

export default logger;
