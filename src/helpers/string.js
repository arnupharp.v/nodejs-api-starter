export function base64Encode(str) {
  const base64 = Buffer.from(str).toString('base64');
  return base64;
}

export function base64Decode(base64) {
  const str = Buffer.from(base64, 'base64').toString('ascii');
  return str;
}

export function stringToSlug(str) {
  let result = str.replace(/^\s+|\s+$/g, ''); // trim
  result = result.toLowerCase();

  // remove accents, swap ñ for n, etc
  const from = 'åàáãäâèéëêìíïîòóöôùúüûñç·/_,:;';
  const to = 'aaaaaaeeeeiiiioooouuuunc------';

  for (let i = 0, l = from.length; i < l; i += 1) {
    result = result.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }

  result = result
    .replace(/[^a-z0-9ก-๛ -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes
    .replace(/^-+/, '') // trim - from start of text
    .replace(/-+$/, ''); // trim - from end of text

  return result;
}

export function stringToBoolean(string) {
  switch (`${string}`.toLowerCase().trim()) {
    case 'true':
    case 'yes':
    case '1':
      return true;
    case 'false':
    case 'no':
    case '0':
    case null:
      return false;
    default:
      return Boolean(string);
  }
}
