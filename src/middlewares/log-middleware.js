import httpContext from 'express-http-context';
import url from 'url';
import randomstring from 'randomstring';
import { unsupportContentType } from '../helpers/error';
import logger from '../helpers/logger';

export default function logMiddleware(app) {
  if (app) {
    app.use(httpContext.middleware);

    // Run the context for each request. Assign a unique identifier to each request
    app.use((req, res, next) => {
      httpContext.set('reqId', randomstring.generate(10));
      next();
    });

    // -> Request log
    app.use((req, res, next) => {
      const [pathname, querystring] = req.originalUrl.split('?');
      const postBody = { ...req.body };
      if (postBody.password) postBody.password = '*';

      logger.info('[REACH]', {
        client: {
          forwarded: req.header('x-forwarded-for'),
          remote: req.connection.remoteAddress,
        },
        method: req.method,
        url: url.format({
          protocol: req.header('x-forwarded-proto') || req.protocol,
          host: req.header('x-forwarded-host') || req.get('Host'),
          pathname,
          search: querystring,
        }),
        httpversion: req.httpVersion,
        agent: req.header('user-agent'),
        referer: req.header('Referer') || '',
        'content-length': req.header('content-length'),
        'content-type': req.header('content-type'),
        query: req.query,
        body: postBody,
      });

      const contentType = req.header('content-type');

      if (
        contentType &&
        !/^(application\/json|multipart\/form-data|application\/x-www-form-urlencoded)/.test(
          contentType,
        )
      ) {
        unsupportContentType(next);
      } else {
        next();
      }
    });
  }
}
