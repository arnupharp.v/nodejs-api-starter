/**
 * Routers middleware.
 * @module middlewares/route
 */

import express from 'express';
import path from 'path';
import fs from 'fs';
import yaml from 'js-yaml';
import route from '../route';

/**
 * Attach routing module to app instance.
 * @param app - Express' app instance.
 * @param express - Express package itself
 */
export default function routeMiddleware(app) {
  // SwaggerUI
  app.use('/swagger', express.static(path.resolve('./swagger')));
  app.use('/swaggerspec', express.static(path.resolve('./swaggerspec')));
  app.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const packageFile = JSON.parse(fs.readFileSync(path.resolve('./package.json')).toString());
    const swagger = yaml.safeLoad(
      fs.readFileSync(`${path.resolve('./swaggerspec/swaggerfile.inc.yaml')}`, 'utf8'),
    );
    const host = req.header('x-forwarded-host') || req.get('host');
    swagger.info = {
      title: packageFile.name,
      version: packageFile.version,
    };
    swagger.servers = [
      {
        url: `http://${host}`,
        description: 'Call Api using http protocol',
      },
      {
        url: `https://${host}`,
        description: 'Call Api using https protocol',
      },
    ];
    res.send(swagger);
  });
  app.use('/docs', express.static(path.resolve('./public')));

  // Main router
  app.use('/', route);
}
