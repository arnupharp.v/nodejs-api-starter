/**
 * Main middleware.
 * @module middlewares/main
 */

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import fileupload from 'express-fileupload';
import express from 'express';

/**
 * Set middlewares to app instance.
 * @param app - Express' app instance.
 */
export default function mainMiddleware(app) {
  app.use(helmet());
  app.use(
    cors({
      origin: process.env.FRONTEND_URL,
      credentials: true,
    }),
  );
  app.use(compression());

  app.use(cookieParser());
  app.use(bodyParser.json({ limit: process.env.MAX_BODY_SIZE }));
  app.use(bodyParser.urlencoded({ extended: false, limit: process.env.MAX_BODY_SIZE }));
  app.use(
    fileupload({
      limits: {
        fileSize: parseInt(process.env.FILEUPLOAD_MAX_FILESIZE, 10),
        files: parseInt(process.env.FILEUPLOAD_MAX_FILES, 10),
      },
      useTempFiles: true,
      tempFileDir: './tmp',
    }),
  );

  app.use(express.static('public'));
}
