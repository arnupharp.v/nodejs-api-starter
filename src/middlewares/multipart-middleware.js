export default function multipartMiddleware(app) {
  if (app) {
    app.use(async (req, res, next) => {
      if (
        req.headers['content-type'] &&
        req.headers['content-type'].toLowerCase().startsWith('multipart/form-data')
      ) {
        if (req.body.jsonBody && typeof req.body.jsonBody === 'string') {
          req.body = JSON.parse(req.body.jsonBody);
        }
      }

      next();
    });
  }
}
