import { idTokenError } from '../helpers/error';

export default function idTokenMiddleware(app) {
  if (app) {
    app.use(async (req, res, next) => {
      // Check for line-id-token
      const provider = req.get('x-token-provider');
      const idToken = req.get('x-id-token');

      if (provider && idToken) {
        // Check if IdToken is valid
        try {
          if (provider === 'line') {
            /* DO SOMETHING WITH LINE TOKEN
            const lineUser = await AuthService.verifyLineIdToken(idToken);
            if (lineUser) {
              req.tokenCredential = {
                provider: provider,
                token: idToken,
                userId: lineUser.sub,
                user: lineUser
              };
            } else {
              throw(errorMessage.INVALID_ID_TOKEN);
            } */
          }
          next();
        } catch (e) {
          console.log(e);
          idTokenError(next);
        }
      } else {
        next();
      }
    });
  }
}
