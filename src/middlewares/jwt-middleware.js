import jwt from 'jsonwebtoken';
import { jwtError } from '../helpers/error';

export default function jwtMiddleware(app) {
  if (app) {
    app.use(async (req, _, next) => {
      const { accessToken } = req.query;
      const auth = req.get('Authorization');

      // access token can be specified in query string or request header.
      if (auth || accessToken) {
        let token = accessToken;

        if (!token) {
          const parts = auth.split(' ');
          if (parts.length === 2) {
            const [bearer, accessToken] = parts;
            // Accept only 'bearer' prefix
            if (bearer && bearer.toLowerCase() === 'bearer') {
              token = accessToken;
            }
          }
        }

        if (token) {
          // Check if payload signature is valid
          try {
            jwt.verify(token, `${process.env.JWT_SECRET}`, (err, payload) => {
              if (err) jwtError(next);
              else {
                req.credential = payload;
              }

              next();
            });
          } catch (e) {
            next(e);
          }
        } else {
          jwtError(next);
        }
      } else {
        next();
      }
    });
  }
}
