const nodeEnv = process.env.NODE_ENV || 'development';

if (nodeEnv === 'development') {
  // eslint-disable-next-line
  require('@babel/register');
}

const path = require('path');
const mdb = require('./knex/knex-mariadb');
// Load environment variable
require('dotenv').config({ path: `.env.${nodeEnv}` });

const { DB_PROVIDER, DB_HOST, DB_PORT, DB_DATABASE, DB_USER, DB_PASSWORD } = process.env;

let provider = DB_PROVIDER;
if (DB_PROVIDER === 'mariadb') {
  provider = mdb;
}

module.exports = {
  development: {
    client: provider,
    connection: {
      host: DB_HOST,
      port: DB_PORT,
      database: DB_DATABASE,
      user: DB_USER,
      password: DB_PASSWORD,
    },
    migrations: {
      directory: path.resolve('./knex/migrations'),
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './knex/seeds',
    },
  },

  staging: {
    client: provider,
    connection: {
      host: DB_HOST,
      port: DB_PORT,
      database: DB_DATABASE,
      user: DB_USER,
      password: DB_PASSWORD,
    },
    migrations: {
      directory: path.resolve('./knex/migrations'),
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './knex/seeds',
    },
  },

  production: {
    client: provider,
    connection: {
      host: DB_HOST,
      port: DB_PORT,
      database: DB_DATABASE,
      user: DB_USER,
      password: DB_PASSWORD,
    },
    migrations: {
      directory: path.resolve('./knex/migrations'),
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './knex/seeds',
    },
  },
};
